declare @bUpdateSS as bit
declare @Inst_ID as int

set @Inst_ID = 106113
set @bUpdateSS = 1 --set to 1 to execute


if @bUpdateSS = 1

begin

update dts_master..securitysessions set lastmodified = dateadd(day,1,lastmodified) where inst_id = @inst_id and dtuser_ID = 1746 --import service
update dts_master..securitysessions set lastmodified = dateadd(day,1,lastmodified) where inst_id = @inst_id and dtuser_ID = 8728 --system service
update dts_master..securitysessions set lastmodified = dateadd(day,1,lastmodified) where inst_id = @inst_id and dtuser_ID = 50027 --Your ID

end
else
begin

select * from dts_master..SecuritySessions where Inst_ID = @Inst_ID

end