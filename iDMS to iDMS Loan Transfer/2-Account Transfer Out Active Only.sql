USE [FEX_001]
GO
/****** Object:  StoredProcedure [dbo].[pr_TransferOut_v10_Accounts_r1]    Script Date: 6/12/2018 6:29:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
declare
	@Transfer_ID INT = 151,  --UPDATE THIS TO YOUR TRANSFER ID BEFORE RUNNING
	@Execute bit = 1 -- if this is 0 it will give a count of accounts  

    
  
declare @Inst_ID int  
declare @BranchList as varchar(250)  
declare @PoolList as varchar(250)  
  
select   
	  
	@Inst_ID = From_Inst_ID,
	@BranchList = From_BranchList,  
	@PoolList = From_PortfolioList  
from  
	FEX_TEMP..Transfer  
where  
	Transfer_ID = @Transfer_ID  
	and Status = 'O'  
  
  
		select  
			@Transfer_ID as Transfer_ID,  
			@Inst_ID as Inst_ID,  
			@BranchList as BranchList,  
			@PoolList as PoolList,  
			count(Acct_ID) as NumberOfAccountsTransfering  
		from   
			Accounts (nolock)  
			  
		where  
			Inst_ID = @Inst_ID  
			and Branch_ID in (select row from dbo.fn_DelimitedStringToRows(isnull(@BranchList,''),','))  
			and Pool_ID in (select row from dbo.fn_DelimitedStringToRows(isnull(@PoolList,''),','))  
			and Status in ('A','O','R') AND AcctType = 'I'
  
if ISNULL(@Inst_ID,0) > 0 and ISNULL(@Execute,0) = 1  
begin  
  
	update  
		FEX_TEMP..Transfer  
	set   
		Status = 'P'   
	where  
		Transfer_ID = @Transfer_ID  
		and Status = 'O'  
  
  
declare @Channel_TO_Count as int  
declare @Person_TO_Count as int  
declare @Account_TO_Count as int  
declare @Collateral_TO_Count as int  
	  
	  
	set @Channel_TO_Count = 0  
  
  
	-- accounts  
	print 'transfer out accounts'  
	insert  
		FEX_TEMP..Transfer_Accounts  
		select  
			@Transfer_ID,  
			'O' as TransferStatus,  
			'' as TransferStatusDesc,  
			0 as New_Account_ID,  
			[Inst_ID],   
			[Acct_ID],   
			[Branch_ID],   
			[Pool_ID],   
			[LoanNumber],   
			[SalesTaxRate],   
			[Status],   
			[BKDate],   
			[ClosedDate],   
			[RepoDate],   
			[OutRepoDate],   
			[InactiveDate],   
			[NoPayDate],   
			[CODate],   
			[TakeNoPay],   
			[ReceiveNotice],   
			[Bankrupt],   
			[LastModified],   
			[AcctType],   
			[FirstPastDueDate],   
			[CreationMethod_id],   
			[Lender_ID],   
			[TicklerDate],   
			[SalesTaxCounty],   
			[TransactionLock],   
			[Deal_ID],   
			[DealWorksheet_ID],   
			[Source_ID],   
			[AlertMessage],   
			[OrigCOBalance],   
			[OrigCOSalesTaxBalance],   
			[CurCOBalance],   
			[CurCOSalesTaxBalance],   
			[COBalanceCollected],   
			[COBalanceAdjusted],   
			[COSalesTaxCollected],   
			[COSalesTaxAdjusted],   
			[SecondaryStatus],   
			[LastNoteDate],   
			[LastNote_ID],   
			[LastMainDate],   
			[LastMain_ID],   
			[LastPromiseDate],   
			[LastPromise_ID],   
			[CurDueDate],   
			[CurDueAmt],   
			[FilteredNumDaysPD],   
			[ActNumDaysPD],   
			[FilteredNumPaymentsPD],   
			[ActNumPaymentsPD],   
			[FilteredStanding],   
			[ActStanding],   
			[FilteredPastDueTier],   
			[ActPastDueTier],   
			[NextDueAmt],   
			[NextDueDate],   
			[LastTransferDate],   
			[ReportCredit],   
			[FilteredDueDate],   
			[FilteredDueAmt],   
			[PassTimeCustomerNum],   
			[ClosingTransactionType],   
			[LastPolicy_ID],   
			[LastRepo_ID],   
			[COBalanceAdjusted_ACV],   
			[Metro2Status],  
			InsNotRequired,  
			FollowupDate,  
			Alert_UseDefaultStyle,  
			Alert_FontColor,  
			Alert_BackgroundColor,  
			0 as LastTransfer_ID,
			LastRTCDate,
			FirstDelinquencyDate
			
		from   
			Accounts (nolock)  
			  
		where  
			Inst_ID = @Inst_ID  
			and Branch_ID in (select row from dbo.fn_DelimitedStringToRows(isnull(@BranchList,''),','))  
			and Pool_ID in (select row from dbo.fn_DelimitedStringToRows(isnull(@PoolList,''),','))  
			and Status in ('A','O','R') AND AcctType = 'I'  
			  
		select @Account_TO_Count = @@ROWCOUNT  
  
		-- acctamendment  
	print 'transfer out acctamendment'  
	insert FEX_TEMP..Transfer_AcctAmendment  
	select  
		@Transfer_ID,  
		0 as New_Acct_ID,  
		0 as New_Balance_ID,  
		0 as New_AmendmentTran_ID,  
		[Amendment_ID],   
		[SnapshotType],   
		[Inst_ID],   
		[Acct_ID],   
		[Balance_ID],   
		[AmendmentDesc],   
		[AmendmentDate],   
		[LoanRate],   
		[RegZAPR],   
		[CurBalance],   
		[CurInterestBalance],   
		[CurSalesTaxBalance],   
		[CurDownPaymentBalance],   
		[CurNonInterestBalance],   
		[DueInterest],   
		[DueLateFees],   
		[CurMiscFeeBalance],   
		[NextAccrualDate],   
		[SystemDate],   
		[SnapshotDate],   
		[AmendmentTran_ID],
		[FinanceCharge]  
	from   
		AcctAmendment (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
		(  
		  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			  
		)  
  
		-- acctamendment  
	print 'transfer out inspolices'  
	insert FEX_TEMP..Transfer_InsPolicies  
	select  
		@Transfer_ID,  
		0 as New_Inventory_ID,  
		0 as New_Acct_ID,  
		0 as New_Policy_ID,  
		0 as New_InsCompany_ID,  
		0 as New_InsAgent_ID,   
		[Inst_ID],   
		[Policy_ID],   
		[InsCompany_ID],   
		[InsAgent_ID],   
		[PolNumber],   
		[ColDeduct],   
		[LiabDeduct],   
		[EffDate],   
		[ExpDate],   
		[Status],   
		[AddDate],   
		[LastModified],   
		[FinalExpDate],   
		[FinalStatusDate],   
		[RenewalPolicy_ID],   
		[UserCode],   
		[Inventory_ID],   
		[Acct_ID],
		[CancelDate]  
	from   
		InsPolicies (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
		(  
		  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			  
		)  
  
	-- ins company channels  
	print 'transfer out ins company channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country 
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				inscompany_id  
			from   
				fex_temp..transfer_inspolicies (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
  
	-- ins company channels  
	print 'transfer out ins agent channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country  
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				insagent_id  
			from   
				fex_temp..transfer_inspolicies (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
  
  
		  
			-- acctbalances  
	print 'transfer out acctbalances'  
		insert FEX_TEMP..Transfer_AcctBalances  
		select  
			@Transfer_ID,  
			'O',  
			'',  
			0,  
			0,  
			[Balance_ID],   
			[Inst_ID],   
			[Acct_ID],   
			[Contract_ID],   
			[BalanceType],   
			[Description],   
			[OpenDate],   
			[OrigMaturityDate],   
			[Term],   
			[RegPayment],   
			[PaymentFrequency],   
			[LoanRate],   
			[RegZAPR],   
			[OrigBalance],   
			[OrigInterestBalance],   
			[OrigSalesTaxBalance],   
			[OrigDownPaymentBalance],   
			[CurBalance],   
			[CurInterestBalance],   
			[CurSalesTaxBalance],   
			[CurDownPaymentBalance],   
			[CurNonInterestBalance],   
			[DueInterest],   
			[DueLateFees],   
			[CurMiscFeeBalance],   
			[AddDate],   
			[LastModified],   
			[NextAccrualDate],   
			[SystemDate],   
			[FlgReminderNotice],   
			[Flg10DayNotice],   
			[Flg15DayNotice],   
			[Flg30DayNotice],   
			[Flg60DayNotice],   
			[Flg90DayNotice],   
			[Flg120DayNotice],   
			[FlgInsReminderNotice],   
			[FlgInsExpNotice],   
			[BalloonTerm],   
			[BalloonPayment],   
			[DispostionFee],   
			[MileageLimit],   
			[ExcessMileageFee],   
			[OrigReserveBalance],   
			[CurReserveBalance],   
			[OrigDiscount],   
			[CurDiscount],   
			[CreditBalance],   
			[LastPaymentDate],   
			[LastTransactionDate],   
			[DiscountMethod_ID],   
			[DiscountPercentage],   
			[CurDueDate],   
			[CurDueAmt],   
			[NextDueDate],   
			[NextDueAmt],   
			[AutoPayFlag],   
			[AutoPay_EPSAccount_GUID],  
			[SalesPrice],  
			[TotalTradeIn],  
			[SalesTaxRate],  
			[AutoPay_ModDay],  
			[AutoPay_AddPayment],  
			[AutoPay_AddPaymentMethod_ID],  
			[BalanceCost],  
			[SalesCost],  
			[CollateralCost],
			0,
			0,
			[SideNoteSubType_ID] 
	from   
		acctbalances (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
	(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
			  
	-- acctbalancesummary  
	print 'transfer out acctbalancesummary'  
	insert FEX_TEMP..Transfer_AcctBalanceSummary  
		select  
		   @Transfer_ID,  
           0 as New_Acct_ID,  
           0 as New_Balance_ID,  
           [Inst_ID],  
           [Acct_ID],  
           [Balance_ID],  
           [LastTransaction_ID],  
           [LastPayment_ID],  
           [DownPayment_Collected],  
           [DownPayment_Increase],  
           [DownPayment_Decrease],  
           [Prin_Collected],  
           [Prin_Increase],  
           [Prin_Decrease],  
           [NonEarnPrin_Collected],  
           [NonEarnPrin_Increase],  
           [NonEarnPrin_Decrease],  
           [Int_Collected],  
           [Int_Increased],  
           [Int_Decreased],  
           [Int_Earned],  
           [LateFee_Collected],  
           [LateFee_Increase],  
           [LateFee_Decrease],  
           [SalesTax_Collected],  
           [SalesTax_Increase],  
           [SalesTax_Decrease],  
           [MiscFee_Collected],  
           [MiscFee_Increase],  
           [MiscFee_Decrease]  
		  
			from   
				AcctBalanceSummary (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Acct_ID in  
			(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
	-- acctinterestschedule  
	print 'transfer out acctinterestschedule'  
	insert FEX_TEMP..Transfer_AcctInterestSchedule  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Balance_ID,  
			[Inst_ID],   
			[Acct_ID],   
			[Balance_ID],   
			[Period],   
			[PeriodBegDate],   
			[PeriodEndDate],   
			[DaysInPeriod],   
			[ScheduledPrinBal],   
			[ScheduledBkInt],   
			[RebatePerDiem]  
	from   
		acctinterestschedule (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
	(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
	-- acctpaymenthistory  
	print 'transfer out acctpaymenthistory'	  
	insert FEX_TEMP..Transfer_AcctPaymentHistory  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			[Inst_ID],   
			[Acct_ID],   
			[Paid_T0],   
			[Paid_T1],   
			[Paid_T2],   
			[Paid_T3],   
			[Paid_T4],   
			[Paid_T5],   
			[Paid_T6],   
			[Paid_T7],   
			[Paid_T8],   
			[Paid_Total]  
	from   
		acctpaymenthistory (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
		(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
	-- acctpaymentschedule  
	print 'transfer out acctpaymentschedule'  
	insert FEX_TEMP..Transfer_AcctPaymentSchedule  
		select  
			@Transfer_ID,  
			'O' as TransferStatus,  
			'' as TransferStatusDesc,  
			0 as New_Acct_ID,  
			0 as New_Balance_ID,  
			0 as New_Payment_ID,  
			0 as New_RefPayment_ID,  
			[Payment_ID],   
			[Inst_ID],   
			[Acct_ID],   
			[Balance_ID],   
			[PayDueDate],   
			[PaymentAmt],   
			[ScheduledPrinPayment],   
			[ScheduledIntPayment],   
			[ScheduledSalesTaxPayment],   
			[LateFeeAssessed],   
			[PaymentType],   
			[Extended],   
			[PrinApplied],   
			[IntApplied],   
			[LateFeeApplied],   
			[SalesTaxApplied],   
			[RefPayment_ID],   
			[Description],   
			[PaymentStatus],   
			[Deleted],   
			[DeletedDate],   
			[PaymentNumber],   
			[PaymentScheduleNumber],   
			[ScheduledDiscountPayment],   
			[DiscountApplied],
			[SkipAutoPay],
			[Comment]
	from   
		acctpaymentschedule (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
	(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
			  
	-- acctpaymentschedules  
	print 'transfer out acctpaymentschedules'		  
	insert FEX_TEMP..Transfer_AcctPaymentSchedules  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Balance_ID,  
			[Inst_ID],   
			[PaymentSchedule_ID],   
			[Acct_Id],   
			[Balance_ID],   
			[PaymentScheduleNumber],   
			[FirstPaymentDate],   
			[NumberOfPayments],   
			[PaymentFrequency],   
			[DesiredPayment],   
			[NumRegPayments],   
			[RegPaymentAmt],   
			[RegTaxPaymentAmt],   
			[NumFinalPayments],   
			[FinalPaymentAmt],   
			[FinalTaxPaymentAmt],   
			[Status],   
			[RegDiscountPaymentAmt],   
			[FinalDiscountPaymentAmt]  
	from   
		acctpaymentschedules (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
	-- acctrepos  
	print 'transfer out acctrepos'  
	insert FEX_TEMP..Transfer_AcctRepos  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_RepoAgent_ID,  
			[Inst_ID],   
			[Repo_ID],   
			[Acct_ID],   
			[RepoAgent_ID],   
			[Note_ID],   
			[PostDate],   
			[PlacedDate],   
			[RepoDate],   
			[CancelDate],   
			[Status],   
			[RepoCharges],   
			[Paid],   
			[PaidRefNumber],   
			[UserCode],   
			[Notes],   
			[OnLot],   
			[OtherLocation],   
			[RepoReason_ID],   
			[FinalStatus],   
			[FinalStatusDate],   
			[DTUser_ID],   
			[RepoMethod_ID],  
			RepoEffectDate, 
			[VehicleLocation_ID]
           ,[AcctReposAddressTypes_ID]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[State]
           ,[Zip]
           ,[PersonalProperty]
           ,[Notified]
           ,[NotifiedOn]
           ,[DaysToClaim]
           ,[ClaimDeadline]
           ,[Description]
           ,[Witness1]
           ,[Witness2]
           ,[Claimed]
           ,[ClaimedDate]
           ,[ClaimedBy] 
	from   
		acctrepos (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
		(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
  
	-- acctrepos  
	print 'transfer out repo channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country  
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				repoagent_id  
			from   
				fex_temp..transfer_acctrepos (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
			  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT		  
	-- acct inventory  
	print 'transfer out acct inventory'  
	insert FEX_TEMP..transfer_inventory  
		select  
		  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Inventory_ID,  
			0 as New_Lot_ID,  
			0 as New_AcquiredChannel_ID,  
			0 as New_FinanceChannel_ID,  
			0 as New_Sale_ID,  
			0 as New_TISale_ID,  
			0 as New_RepoAcct_ID,  
			0 as New_AcquiredFrom_ID,  
			0 as New_EvaluationGroup_ID,  
			[Inventory_ID],   
			[Inst_ID],   
			[InvType_ID],   
			[StockNumber],   
			[NewFlag],   
			[Year],   
			[Make],   
			[Model],   
			[BodyStyle],   
			[ExtColor],   
			[ExtTrim],   
			[IntColor],   
			[VIN],   
			[LicPlates],   
			[LicPlatesExpire],   
			[ValNumber],   
			[InspectExpire],   
			[Mileage],   
			[MileageStatus_ID],   
			[Condition_ID],   
			[Transmission],   
			[DriveType],   
			[Engine],   
			[Comments],   
			[AskPrice],   
			[AskDown],   
			[AskTerm],   
			[ImageMain],   
			[ImageBack],   
			[ImageSide],   
			[ImageInterior],   
			[Status],   
			[Sale_ID],   
			[AcquiredPrice],   
			[AcquiredDate],   
			[AcquiredMethod],   
			[AcquiredMileage],   
			[AcquiredMileageStatus_ID],   
			[AcquiredChannel_ID],   
			[AcquiredRefNumber],   
			[PurchaseStatus],   
			[TI_Sale_ID],   
			[TI_SaleItem_ID],   
			[TI_Payoff],   
			[TI_AccountNumber],   
			[TI_CustomerName],   
			[TI_CompanyName],   
			[TI_Add1],   
			[TI_Add2],   
			[TI_City],   
			[TI_State],   
			[TI_Zip],   
			[TI_Contact],   
			[TI_Phone],   
			[TI_Fax],   
			[TI_Email],   
			[FinanceChannel_ID],   
			[FinanceDate],   
			[FinanceAmount],   
			[FinanceCloseDate],   
			[FinanceCloseRefNumber],   
			[Location],   
			[TitleLocation],   
			[ClearTitle],   
			[DescTitleProblem],   
			[DateInvTaxPaid],   
			[LastModified],   
			[AddDate],   
			[ASIS],   
			[WholesalePrice],   
			[DateToAuction],   
			[AuctionName],   
			[AuctionLane],   
			[AuctionNumber],   
			[Lot_ID],   
			[InspExpireMth],   
			[InspExpireYear],   
			[LicPlatesExpireMth],   
			[LicPlatesExpireYear],   
			[RepoAcct_ID],   
			[Buyer_ID],   
			[PakFee],   
			[ListOnWeb],   
			[Acct_ID],   
			[FinanceDueDate],   
			[InvRecoveryDevice],   
			[YearPreviousSale],   
			[Weight],   
			[Notes],   
			[WarrantyTerms],   
			[InvRecoveryDeviceType_ID],   
			[InstalledOptions],   
			[FuelType],   
			[Stereo],   
			[ListOnAutoTrader],   
			[Image1],   
			[Image2],   
			[Image3],   
			[Image4],   
			[Image5],   
			[Image6],   
			[Image7],   
			[Image8],   
			[Image9],   
			[Image10],   
			[Image11],   
			[Image12],   
			[Image13],   
			[Image14],   
			[Image15],   
			[Image16],   
			[Image17],   
			[Image18],   
			[Image19],   
			[Image20],   
			[Image21],   
			[Image22],   
			[Image23],   
			[Image24],   
			[Image25],   
			[LastDateListedWeb],   
			[LastDateListedAutoTrader],   
			[PromotionalPrice],   
			[PromotionalPayment],   
			[PromotionalTerm],   
			[SpecialFinancePrice],   
			[SpecialFinancePayment],   
			[SpecialFinanceTerm],   
			[Residual24],   
			[Residual36],   
			[Residual48],   
			[Residual60],   
			[FMV],   
			[ACV],   
			[CRV],   
			[Taxes],   
			[InitialChargeOff],   
			[OwnerDivision],   
			[AlternateLot_ID],   
			[ListOnCarsCom],   
			[LastDateListedCarsCom],   
			[AskPrice_Low],   
			[CarsComImageChanged],   
			[TempDealerTag],   
			[TempDealerTagIssueDate],   
			[TempSalesTag],   
			[TempSalesTagIssueDate],   
			[TempSalesExtTag],   
			[TempSalesExtTagIssueDate],   
			[OnTimeContract_ID],   
			[TitleNumber],   
			[EmmissionsNumber],   
			[InvCreationMethod_ID],   
			[SellerNotes],   
			[ExtColorCommon],   
			[ImageCount],   
			[InternetSpecial],   
			[VROXCertificate],   
			[Inspected],   
			[InvRecoverySubDeviceType_ID],   
			[CertCode],   
			[CertVendor_ID],   
			[CertStatus],   
			[CertDate],   
			[DeletedDate],   
			[AcquiredFromType_ID],   
			[AcquiredFrom_ID],   
			[TotalExpenses],   
			[EvaluationGroup_ID],   
			[EvaluationDate],   
			[EvaluationOfferPrice],   
			[EvaluationBidPrice],   
			[EvaluationBidExpiration],   
			[EvaluationLane],   
			[EvaluationNumber],   
			[ExtPrimaryCommonColor],   
			[ExtSecondaryCommonColor],   
			[AutoCheck_Requested],
			InvRecoveryDeviceExpDate,
			LastStatus,
			SiriusStatus_ID,
			ToLocationDate,
			StatusChangeDate
	from   
		inventory (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Acct_ID in  
	(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
	select @Collateral_TO_Count = @@ROWCOUNT		  
	-- acquiredchannels  
	print 'transfer out acquired channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country   
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				AcquiredChannel_ID  
			from   
				fex_temp..transfer_inventory (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
	  
	-- acquiredchannels  
	print 'transfer out finance channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country  
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				FinanceChannel_ID  
			from   
				fex_temp..transfer_inventory (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
  
	-- accounts inventoryequipment  
	print 'transfer out inventoryequipment'  
	insert FEX_TEMP..transfer_inventoryequipment  
		select  
			@Transfer_ID,  
			0 as New_Inventory_ID,  
			[Inst_ID],   
			[Inventory_ID],   
			[Equipment_Id],   
			[CustomDesc]  
	from   
		inventoryequipment (nolock)  
	where  
		Inst_ID = @Inst_ID   
		and inventory_ID in  
		(  
		select  
			Inventory_ID  
		from   
			fex_temp..transfer_inventory (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		)  
	  
  
	-- inventoryservice  
	print 'transfer out inventoryservice'  
	insert FEX_TEMP..transfer_inventoryservice  
		select  
			@Transfer_ID,  
			0 as New_Inventory_ID,  
			0 as New_ServiceComp_ID,  
			[InvService_ID],   
			[Inst_ID],   
			[Inventory_ID],   
			[ServiceComp_ID],   
			[ServiceDate],   
			[PostDate],   
			[PostedBy],   
			[Estimate],   
			[FinalCost],   
			[Status],   
			[RefNumber],   
			[Payment],   
			[PayRefNumber],   
			[Paid],   
			[PaidDate],   
			[PaidBy],   
			[Description],   
			[ExportGUID],   
			[ClassCode],   
			[DTUser_ID],  
			isnull(deleted,0),
			PaymentForm_ID  
	from   
		inventoryservice (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and inventory_ID in  
		(  
		select  
			Inventory_ID  
		from   
			fex_temp..transfer_inventory (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		)  
	  
	  
		-- acquiredchannels  
	print 'transfer out service company channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country   
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				ServiceComp_ID  
			from   
				fex_temp..transfer_inventoryservice (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
	  
	-- inventorytitle  
	print 'transfer out inventorytitle'  
	insert FEX_TEMP..transfer_inventorytitle  
		select  
			@Transfer_ID,  
			0 as New_Inventory_ID,  
			0 as New_TitleLocation_ID,  
			[Title_ID],   
			[Inst_ID],   
			[Inventory_ID],   
			[TitleStatus_ID],   
			[TitleComments],   
			[TitleLocation_ID],   
			[TitleNumber],   
			[COTName],   
			[COTAdd1],   
			[COTAdd2],   
			[COTCity],   
			[COTCounty],   
			[COTState],   
			[COTZip],   
			[COTCountry],   
			[COTPhone1],   
			[COTFax1],   
			[LienAcct_ID],   
			[LienBalance],   
			[LienName],   
			[LienAcctNumber],   
			[LienContact],   
			[LienAdd1],   
			[LienAdd2],   
			[LienCity],   
			[LienCounty],   
			[LienState],   
			[LienZip],   
			[LienCountry],   
			[LienPhone1],   
			[LienFax1],   
			[LastModified],   
			[AddDate],   
			[AltTitleLocation],
			VendorChannel_ID,
			PrevTitleTransferred,
			PrevTitleSent,
			PrevTitleNumber,
			PrevTitleState,
			PrevPostalCode,
			PrevCity,
			PrevState,
			PrevCounty,
			PrevTitleReceived
	from   
		inventorytitle (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and inventory_ID in  
		(  
		select  
			Inventory_ID  
		from   
			fex_temp..transfer_inventory (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		)  
	  
  
	-- note  
	print 'transfer out note'  
	insert FEX_TEMP..Transfer_Note  
		select  
			@Transfer_ID,  
			0 as New_Note_ID,  
			0 as New_Record_ID,  
			0 as New_SubRecord_ID,  
			0 as New_LargeNote_ID,  
			[Inst_ID],   
			[Note_ID],   
			[DTUser_ID],   
			[UserCode],   
			[RecordType_ID],   
			[Record_ID],   
			[SubRecordType_ID],   
			[SubRecord_ID],   
			[NoteDate],   
			[Note],   
			[Action],   
			[Result],   
			[PromiseType],   
			[PromiseDueDate],   
			[PromiseAmount],   
			[PromiseStatus],   
			[WorkStartTime],   
			[WorkEndTime],   
			[LastModified],   
			[NoteLarge_ID],  
			PromisePaymentMethod,  
			PromiseDeliveryMethod_ID  
	from   
		note (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and recordtype_id = 4  
		and record_id in  
		(  
		select  
			acct_id  
		from   
			fex_temp..transfer_accounts (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		)  
		  
		  
	-- notelarge  
	print 'transfer out notelarge'	  
	insert FEX_TEMP..Transfer_NoteLarge  
		select  
			@Transfer_ID,  
			0 as New_LargeNote_ID,  
			[NoteLarge_ID],   
			[Inst_ID],   
			[Note]  
	from   
		notelarge (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and notelarge_id in  
		(  
		select  
			NoteLarge_ID  
		from   
			fex_temp..transfer_note (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		)  
  
  
	-- TransactionAccountSnapshot  
	print 'transfer out TransactionAccountSnapshot'  
	insert FEX_TEMP..Transfer_TransactionAccountSnapshot  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Tran_ID,  
			0 as New_CollectionOfficer_ID,  
			[Inst_ID],   
			[Acct_ID],   
			[Tran_ID],   
			[SnapshotType],   
			[PL_CurDownPaymentBalance],   
			[PL_CurPrinBalance],   
			[PL_CurNonEarnPrinBalance],   
			[PL_CurSalesTaxBalance],   
			[PL_CurUnEarnedInterestBalance],   
			[PL_CurMiscFeeBalance],   
			[PL_CurDiscountBalance],   
			[PL_DueInterest],   
			[PL_DueLateFees],   
			[SL_CurPrinBalance],   
			[SL_CurUnEarnedInterestBalance],   
			[SL_CurMiscFeeBalance],   
			[SL_DueInterest],   
			[SL_DueLateFees],   
			[IL_CurPrinBalance],   
			[CO_CurBalance],   
			[CO_CurSalesTaxBalance],   
			[ActCurDueDate],   
			[ActCurDueAmt],   
			[ActStanding],   
			[ActPastDueTier],   
			[ActNumDaysPD],   
			[ActNumPaymentsPD],   
			[FilteredCurDueDate],   
			[FilteredCurDueAmt],   
			[FilteredStanding],   
			[FilteredPastDueTier],   
			[FilteredNumDaysPD],   
			[FilteredNumPaymentsPD],   
			[NextDueDate],   
			[NextDueAmt],   
			[Status],   
			[CollectionOfficer_ID]  
		from   
			transactionaccountsnapshot (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and acct_id in  
			(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
	-- Transactions  
	print 'transfer out Transactions'  
	insert FEX_TEMP..Transfer_Transactions  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Tran_ID,  
			0 as New_Pool_ID,  
			0 as New_Source_ID,  
			0 as New_Branch_ID,  
			0 as New_TranRef,  
			0 as New_RevTran_Id,  
			[Inst_ID],   
			[Tran_ID],   
			[Acct_ID],   
			[Batch_ID],   
			[Pool_ID],   
			[Source_ID],   
			[Sales_ID],   
			[Cus_ID],   
			[UserCode],   
			[PostDate],   
			[EffectDate],   
			[TranCode],   
			[GenType],   
			[Form],   
			[RefNumber],   
			[TranRef],   
			[Cleared],   
			[Comment],   
			[StatusBef],   
			[StatusAfter],   
			[StandingBef],   
			[StandingAfter],   
			[NumDueDates],   
			[FinalPartial],   
			[DueDateBef],   
			[DueDateAfter],   
			[LoanBalBef],   
			[LoanPayoffBef],   
			[LoanBalAfter],   
			[LoanPayoffAfter],   
			[NumRegPayDueBef],   
			[RegPayDueBef],   
			[NumPartPayDueBef],   
			[PartPayDueBef],   
			[NumRegPayDueAfter],   
			[RegPayDueAfter],   
			[NumPartPayDueAfter],   
			[PartPayDueAfter],   
			[Branch_ID],   
			[AmountPaid],   
			[AmountReceived],   
			[ChangeGiven],   
			[CurDueAmtBef],   
			[CurDueAmtAfter],   
			[AcctBalanceAfter],   
			[AcctBalanceBef],   
			[NextDueAmtAfter],   
			[NextDueAmtBef],   
			[NextDueDateAfter],   
			[NextDueDateBef],   
			[ExportGUID],   
			[DTUser_ID],   
			[RevTran_ID],   
			[TranStatus],   
			[EPSTransaction_GUID],   
			[DeliveryMethod_ID],
			Queue_ID 
		from   
			transactions (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and acct_id in  
			(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
	-- TransactionSummary  
	print 'transfer out TransactionSummary'  
	insert FEX_TEMP..Transfer_TransactionSummary  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Tran_ID,  
			[TranSummary_ID],   
			[Tran_ID],   
			[Inst_ID],   
			[Acct_ID],   
			[TotalTransactionAmount],   
			[TotalTransactionAdjustment],   
			[TotalTransactionTransfer],   
			[TotalTransactionCollected],   
			[TransactionSalesTaxAdjustment],   
			[TransactionSalesTaxTransfer],   
			[TransactionSalesTaxCollected],   
			[TransactionDownPaymentAdjustment],   
			[TransactionDownPaymentTransfer],   
			[TransactionDownPaymentCollected],   
			[TransactionPrinAdjustment],   
			[TransactionPrinTransfer],   
			[TransactionPrinCollected],   
			[TransactionNonIntPrinAdjustment],   
			[TransactionNonIntPrinTransfer],   
			[TransactionNonIntPrinCollected],   
			[TransactionOtherPrinAdjustment],   
			[TransactionOtherPrinTransfer],   
			[TransactionOtherPrinCollected],   
			[TransactionCPIAdjustment],   
			[TransactionCPITransfer],   
			[TransactionCPICollected],   
			[TransactionIntDueAdjustment],   
			[TransactionIntDueTransfer],   
			[TransactionIntDueCollected],   
			[TransactionUnearnIntAdjustment],   
			[TransactionUnearnedIntTransfer],   
			[TransactionLateFeeAdjustment],   
			[TransactionLateFeeTransfer],   
			[TransactionLateFeeCollected],   
			[TransactionMiscFeeAdjustment],   
			[TransactionMiscFeeTransfer],   
			[TransactionMiscFeeCollected],   
			[TransactionOtherIntDueAdjustment],   
			[TransactionOtherIntDueTransfer],   
			[TransactionOtherIntDueCollected],   
			[TransactionCPIIntDueAdjustment],   
			[TransactionCPIIntDueTransfer],   
			[TransactionCPIIntDueCollected],   
			[TransactionDiscountAdjustment],   
			[TransactionDiscountTransfer],   
			[TransactionDiscountCollected],   
			[TransactionChargeOffAdjustment],   
			[TransactionChargeOffTransfer],   
			[TransactionChargeOffCollected],  
			[TransactionConvenienceFeeadjustment],  
			[TransactionConvenienceFeeTransfer],  
			[TransactionConvenienceFeeCollected]  
		from   
			transactionsummary (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and acct_id in  
			(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
	-- TranSplits  
	print 'transfer out TranSplits'  
	insert FEX_TEMP..Transfer_TranSplits  
		select  
			@Transfer_ID,  
			0 as New_Tran_ID,  
			0 as New_Payment_ID,  
			0 as New_Balance_ID,  
			[Split_ID],   
			[Tran_ID],   
			[ClassCode],   
			[Amount],   
			[BalBefore],   
			[BalAfter],   
			[GenType],   
			[RefPaymentDate],   
			[PaymentRelated],   
			[Payment_ID],   
			[Balance_ID]  
		from   
			transplits (nolock)  
		where  
			tran_id in  
			(  
			select  
				tran_id  
			from   
				fex_temp..transfer_transactions (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
			  
			  
			  
			  
	-- person account  
	print 'transfer out PERSONACCOUNT'  
	insert FEX_TEMP..Transfer_PersonAccount  
		select  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Person_ID,  
			[Inst_ID],   
			[PersonAccount_ID],   
			[Person_ID],   
			[Acct_ID],   
			[BorrowerType_ID],   
			[OrderNumber],
			ConsumerInformationIndicator,
			ECOACode,
			BK_Petition_Notification,
			Bankrupt			  
		from   
			personaccount (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and acct_id in  
			(  
			select  
				acct_id  
			from   
				fex_temp..transfer_accounts (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
		  
	---- person reference  
	--print 'transfer out PERSONREFERENCE'  
	--insert FEX_TEMP..Transfer_PersonReference  
	--	select  
	--		@Transfer_ID,  
	--		0 as New_Person_ID,  
	--		0 as New_Reference_ID,  
	--		[Inst_ID],   
	--		[PersonReference_ID],   
	--		[Person_ID],   
	--		[Reference_ID],   
	--		[ReferenceType_ID],
	--		IsAuthorized3rdParty  
	--	from   
	--		personreference (nolock)  
	--	where  
	--		Inst_ID = @Inst_ID  
	--		and person_id in  
	--		(  
	--		select  
	--			person_id  
	--		from   
	--			fex_temp..transfer_personaccount (nolock)  
	--		where  
	--			Inst_ID = @Inst_ID  
	--			and Transfer_ID = @Transfer_ID  
	--		)  
			  
			  
	--	-- select the reverse selection  
	--	select  
	--		@Transfer_ID,  
	--		0 as New_Person_ID,  
	--		0 as New_Reference_ID,  
	--		[Inst_ID],   
	--		[PersonReference_ID],   
	--		[Person_ID],   
	--		[Reference_ID],   
	--		[ReferenceType_ID],
	--		IsAuthorized3rdParty 
	--	from   
	--		personreference (nolock)  
	--	where  
	--		Inst_ID = @Inst_ID  
	--		and person_id in  
	--		(  
	--		select  
	--			Reference_ID  
	--		from   
	--			fex_temp..transfer_personreference (nolock)  
	--		where  
	--			Inst_ID = @Inst_ID  
	--			and Transfer_ID = @Transfer_ID  
	--		)  
  
	-- person  
	print 'transfer out PERSON'  
	insert FEX_TEMP..Transfer_Person  
		select  
			@Transfer_ID,  
			0 as New_Person_ID,  
			[Inst_ID],   
			[Person_ID],   
			[SSN],   
			[Salutation],   
			[LName],   
			[FName],   
			[MName],   
			[Suffix],   
			[Gender],   
			[DlNumber],   
			[DlState],   
			[DlExpire],   
			[DOB],   
			[Email],   
			[Password],   
			[KeyQuestion],   
			[KeyAnswer],   
			[AddDate],   
			[LastModified],   
			[OtherUID],   
			[Last8300Filing],   
			[LastOFACRequest],   
			[CreditScore],   
			[CreditScoreType_ID],   
			[CreditScoreDate],   
			[DoNotSolicit],   
			[DoNotSolicit_Joint],   
			[DoNotSolicit_Aff],   
			[DoNotSolicit_NonAff],   
			[DoNotShareHistory_Aff],   
			[DoNotShareCredit_Aff]
			,[ActiveServiceMember]  
		   ,[AccountOptOut]
           ,[MarketingOptOut]
           ,[AccountOptOutDate]
           ,[MarketingOptOutDate]
           ,[AlternateIdTypeId]
           ,[AlternateIdNumber]
           ,[AlternateIdState]
           ,[AlternateIdCountry]
           ,[IsBusiness]
           ,[BusinessName]
           ,[BusinessDBA]
           ,[BusinessFedId]
           ,[EscrowBalance]
           ,[PreviousCustomer_ID]
           ,[DLIssueDate]
		   ,DSEntityId
           
		from   
			person (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and person_id in  
			(  
			select  
				person_id  
			from   
				fex_temp..transfer_personaccount (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
	select @Person_TO_Count = @@ROWCOUNT  
  
	-- transfer reference person  
	insert FEX_TEMP..Transfer_Person  
		select  
			@Transfer_ID,  
			0 as New_Person_ID,  
			[Inst_ID],   
			[Person_ID],   
			[SSN],   
			[Salutation],   
			[LName],   
			[FName],   
			[MName],   
			[Suffix],   
			[Gender],   
			[DlNumber],   
			[DlState],   
			[DlExpire],   
			[DOB],   
			[Email],   
			[Password],   
			[KeyQuestion],   
			[KeyAnswer],   
			[AddDate],   
			[LastModified],   
			[OtherUID],   
			[Last8300Filing],   
			[LastOFACRequest],   
			[CreditScore],   
			[CreditScoreType_ID],   
			[CreditScoreDate],   
			[DoNotSolicit],   
			[DoNotSolicit_Joint],   
			[DoNotSolicit_Aff],   
			[DoNotSolicit_NonAff],   
			[DoNotShareHistory_Aff],   
			[DoNotShareCredit_Aff],  
			[AccountOptOut]
			,[ActiveServiceMember]
           ,[MarketingOptOut]
           ,[AccountOptOutDate]
           ,[MarketingOptOutDate]
           ,[AlternateIdTypeId]
           ,[AlternateIdNumber]
           ,[AlternateIdState]
           ,[AlternateIdCountry]
           ,[IsBusiness]
           ,[BusinessName]
           ,[BusinessDBA]
           ,[BusinessFedId]
           ,[EscrowBalance]
           ,[PreviousCustomer_ID]
           ,[DLIssueDate]
		   
		   ,DSEntityId
            
		from   
			person (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and person_id in  
			(  
			select  
				reference_id  
			from   
				fex_temp..transfer_personreference (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
			and  
			person_id not in  
			(select  
				person_id  
			from   
				fex_temp..transfer_person (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
			  
	select @Person_TO_Count = @Person_TO_Count + @@ROWCOUNT  
  
	  
  
	-- person address  
	print 'transfer out PERSONADDDRESS'  
	insert FEX_TEMP..Transfer_PersonAddress  
		select  
			@Transfer_ID,  
			0 as New_Person_ID,  
			[Inst_ID],   
			[Person_ID],   
			[Address_ID],   
			[AddressStatus],   
			[AddressStatusDate],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Country],   
			[ResType],   
			[ResDate],   
			[ResLienHolder],   
			[ResLienHolderAdd1],   
			[ResLienHolderAdd2],   
			[ResLienHolderCity],   
			[ResLienHolderCounty],   
			[ResLienHolderState],   
			[ResLienHolderZip],   
			[ResLienHolderCountry],   
			[ResLienHolderPhone],   
			[AddDate],   
			[LastModified],   
			[Verified],   
			[VerifiedBy],   
			[VerifiedMethod],   
			[ResMonthlyPayment],   
			[TimeAtAddress_Years],   
			[TimeAtAddress_Months]  
		from   
			personaddress (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and person_id in  
			(  
			select  
				person_id  
			from   
				fex_temp..transfer_person (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
  
	-- person employment  
	print 'transfer out PERSONEMPLOYMENT'  
	insert FEX_TEMP..Transfer_PersonEmployment  
		select  
			@Transfer_ID,  
			0 as New_Person_ID,  
			[Inst_ID],   
			[Person_ID],   
			[Employment_ID],   
			[EmploymentType],   
			[EmploymentStatus],   
			[EmploymentStatusDate],   
			[Employer],   
			[Position],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Country],   
			[EmploymentDate],   
			[AddDate],   
			[LastModified],   
			[Verified],   
			[VerifiedBy],   
			[VerifiedMethod],   
			[MonthlyIncome],   
			[SupervisorPhone],   
			[TimeAtEmployment_Years],   
			[TimeAtEmployment_Months],
			Income,
			IncomeFrequency,
			Email  
		from   
			personemployment (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and person_id in  
			(  
			select  
				person_id  
			from   
				fex_temp..transfer_person (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
  
  
	-- person phone  
	print 'transfer out PERSONPHONE'  
	insert FEX_TEMP..Transfer_PersonPhone  
		select  
			@Transfer_ID,  
			0 as New_Person_ID,  
			[Inst_ID],   
			[Person_ID],   
			[Phone_ID],   
			[PhoneType_ID],   
			[PhoneNumber],   
			[Extension],   
			[PhoneStatus],   
			[PhoneStatusDate],   
			[Added],   
			[LastModified],   
			[Verified],   
			[VerifiedBy],   
			[VerifiedMethod],  
			[AllowMarketingText],  
			[AllowMarketingTextDate],  
			[AllowAccountingText],  
			[AllowAccountingTextDate],
			DoNotCall,
			DoNotCallDate  
		from   
			personphone (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and person_id in  
			(  
			select  
				person_id  
			from   
				fex_temp..transfer_person (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		  
			  
			  
	update   
		FEX_TEMP..Transfer  
	set  
		Status = 'TI',  
		Channel_TO_Count = @Channel_TO_Count,  
		Person_TO_Count = @Person_TO_Count,  
		Account_TO_Count = @Account_TO_Count,  
		Collateral_TO_Count = @Collateral_TO_Count  
	where  
		Transfer_ID = @Transfer_ID  
		  
			  
end  
		  
		  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
