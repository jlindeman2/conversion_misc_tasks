﻿--Institution Reset; Deletes data permanently, use with caution
--If a Foreign Key error occurs, run the reset again

--ONLY RUN THIS TOP PART FIRST-------------------------------------------------------------------

DECLARE @Inst_ID AS INT;
DECLARE @ModifiedBy_ID INT;

--**Set Institution to be reset, along with your DTUser_ID and run ONLY the below select statement first**
SET @Inst_ID = 1;
SET @ModifiedBy_ID = 0 --CHANGE TO YOUR DTUSER_ID FROM DTUSER IN DTS_MASTER

--**SET SQL CATALOG TO THE CATALOG RETURNED BY THIS SELECT QUERY**
SELECT Inst_id,Name,SQLCatalog FROM DTS_MASTER..INSTITUTIONS WHERE INST_ID = @INST_ID

----------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------
--NOW RUN EVERYTHING AFTER SETTING THE BELOW DELETE BIT VARIABLES TO THE DATA YOU WANT DELETED
----------------------------------------------------------------------------------------------

DECLARE @DeleteChannels AS BIT = 0;
DECLARE @DeletePersons AS BIT = 0;
DECLARE @DeleteInventory AS BIT = 0;
DECLARE @DeleteAccounts AS BIT = 0;
DECLARE @DeleteDeals AS BIT = 0;
DECLARE @DeleteInvListings AS BIT = 0;
DECLARE @ChangeAudit VARCHAR(250);


--**Set to 1 to execute the deletion of data**
--** Only uncomment the lines for what should be deleted**
--SET @DeleteChannels =    1;
--SET @DeletePersons =    1;
--SET @DeleteInventory =      1;
--SET @DeleteAccounts =    1;
--SET @DeleteDeals =        1;
--SET @DeleteInvListings =  1;

IF isnull(@ModifiedBy_ID, 0) > 0
    BEGIN
        IF isnull(@Inst_ID, 0) > 0
            BEGIN  

                --******************
                --Channels
                --******************
                IF ISNULL(@DeleteChannels, 0) = 1
                    BEGIN
            DELETE
            FROM channels
                        WHERE channel_id IN
                        (
                            SELECT record_id
                            FROM note(nolock)
                            WHERE note_id IN
                            (
                                SELECT MAX(note_id)
                                FROM note(nolock)
                                GROUP BY record_id
                            )
                            AND inst_id = @Inst_ID
                            AND recordtype_id = 8
                            AND dtuser_id = 1746
                        )
                    AND inst_id = @Inst_ID;
          END
                --******************
                --Person
                --******************
                IF ISNULL(@DeletePersons, 0) = 1
                    BEGIN
                        DELETE person
                        WHERE inst_id = @Inst_ID;
                        DELETE personphone
                        WHERE inst_id = @Inst_ID;
                        DELETE personemployment
                        WHERE inst_id = @Inst_ID;
                        DELETE personaddress
                        WHERE inst_id = @Inst_ID;
                        DELETE personaccount
                        WHERE inst_id = @Inst_ID;
                        DELETE person_ofac
                        WHERE inst_id = @Inst_ID;
                        DELETE personreference
                        WHERE inst_id = @Inst_ID;
            DECLARE @RowcountPerson INT = 1
            WHILE @RowcountPerson > 0
            BEGIN
            DELETE TOP (1000) 
            FROM note
            WHERE inst_id = @Inst_ID
                        AND recordtype_id = 3;
            SET @RowcountPerson = @@ROWCOUNT
            END
                    END;
                ELSE
                    BEGIN
                        SELECT *
                        FROM person
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM personphone
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM personemployment
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM personaddress
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM personaccount
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM person_ofac
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM personreference
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM note
                        WHERE inst_id = @Inst_ID
                              AND recordtype_id = 3;
                    END;
                --******************
                --Inventory
                --******************
                IF ISNULL(@DeleteInventory, 0) = 1
                    BEGIN
                        --Inventory Expenses
                        DELETE inventoryservice
                        WHERE inst_id = @Inst_ID; 
                        --Inventory Transactions
                        DELETE inventorytransactions
                        WHERE inst_id = @Inst_ID; 
                        --Available Inventory
                        DELETE inventory
                        WHERE inst_id = @Inst_ID
                              AND Status <> 's';
                        DELETE inventorybuyersguide
                        WHERE inst_id = @Inst_ID;
                    END;
                ELSE
                    BEGIN
                        SELECT *
                        FROM inventoryservice
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM inventorytransactions
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM inventory
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM inventorybuyersguide
                        WHERE inst_id = @Inst_ID;
                    END;
                --******************
                --Accounts
                --******************
                IF ISNULL(@DeleteAccounts, 0) = 1
                    BEGIN
                        DELETE acctbalances
                        WHERE inst_id = @Inst_ID;
                        DELETE accounts
                        WHERE inst_id = @Inst_ID;
            DECLARE @RowcountAccountNote INT = 1
            WHILE @RowcountAccountNote > 0
            BEGIN
            DELETE TOP (1000) 
            FROM note
            WHERE inst_id = @Inst_ID
                        AND recordtype_id = 4;
            SET @RowcountAccountNote = @@ROWCOUNT
            END
                        DELETE inventory
                        WHERE acct_id <> 0
                              AND status = 'S'
                              AND inst_id = @Inst_ID;
                        --Account Payment Schedule
            DECLARE @RowcountAcctIntSched INT = 1
            WHILE @RowcountAcctIntSched > 0
            BEGIN
            DELETE TOP (1000) 
            FROM acctinterestschedule
            WHERE inst_id = @Inst_ID
            SET @RowcountAcctIntSched = @@ROWCOUNT
            END
            DECLARE @RowcountAcctPaySched INT = 1
            WHILE @RowcountAcctPaySched > 0
            BEGIN
            DELETE TOP (1000) 
            FROM acctpaymentschedule
            WHERE inst_id = @Inst_ID
            SET @RowcountAcctPaySched = @@ROWCOUNT
            END
                        DELETE acctpaymentschedules
                        WHERE inst_id = @Inst_ID; 
                        --Transactions
            DECLARE @RowcountTransplits INT = 1
            WHILE @RowcountTransplits > 0
            BEGIN
            DELETE TOP (1000) 
            FROM dbo.TranSplits
            WHERE tran_id IN
                        (
                            SELECT tran_id
                            FROM transactions
                            WHERE inst_id = @Inst_ID
                        );
            SET @RowcountTransplits = @@ROWCOUNT
            END
                        DELETE transplits
                        WHERE tran_id IN
                        (
                            SELECT tran_id
                            FROM transactions
                            WHERE inst_id = @Inst_ID
                        );
            DECLARE @RowcountTrans INT = 1
            WHILE @RowcountTrans > 0
            BEGIN
            DELETE TOP (1000) 
            FROM transactions
            WHERE inst_id = @Inst_ID
            SET @RowcountTrans = @@ROWCOUNT
            END
                    END;
                ELSE
                    BEGIN
                        SELECT *
                        FROM acctinterestschedule
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM acctpaymentschedule
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM acctpaymentschedules
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM transplits
                        WHERE tran_id IN
                        (
                            SELECT tran_id
                            FROM transactions
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM transactions
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM personaccount
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM acctbalances
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM accounts
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM note
                        WHERE inst_id = @Inst_ID
                              AND recordtype_id = 4;
                        SELECT *
                        FROM inventory
                        WHERE acct_id <> 0
                              AND status = 'S'
                              AND inst_id = @Inst_ID;
                    END;
                --******************
                --Deals
                --******************
                IF ISNULL(@DeleteDeals, 0) = 1
                    BEGIN
                        DELETE dealtradein
                        WHERE deal_id IN
                        (
                            SELECT deal_id
                            FROM deals
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheetcontractfee
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheetdefdown
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheetinventory
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheetpaymentschedule
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheetproduct
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheettax
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheettradein
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealworksheetpaymentschedule_xproducts
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        DELETE dealaudits
                        WHERE inst_id = @Inst_ID;
                        DELETE dealborrowers
                        WHERE inst_id = @Inst_ID;
                        DELETE dealinventory
                        WHERE inst_id = @Inst_ID;
                        DELETE dealpaymentschedules
                        WHERE inst_id = @Inst_ID;
                        DELETE dealpersons
                        WHERE inst_id = @Inst_ID;
                        DELETE dealsends
                        WHERE inst_id = @Inst_ID;
                        DELETE dealtaxes
                        WHERE inst_id = @Inst_ID;
                        DELETE dealtransactions
                        WHERE inst_id = @Inst_ID;
                        DELETE dealtransplits
                        WHERE inst_id = @Inst_ID;
                        DELETE dealwarrantycontract
                        WHERE inst_id = @Inst_ID;
                        DELETE dealwarrantyrate
                        WHERE inst_id = @Inst_ID;
                        DELETE dealwarrantyrateoptions
                        WHERE inst_id = @Inst_ID;
                        DELETE dealworksheet
                        WHERE inst_id = @Inst_ID;
                        DELETE deals
                        WHERE inst_id = @Inst_ID;
                    END;
                ELSE
                    BEGIN
                        SELECT *
                        FROM dealtradein
                        WHERE deal_id IN
                        (
                            SELECT deal_id
                            FROM deals
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheetcontractfee
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheetdefdown
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheetinventory
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheetpaymentschedule
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheetproduct
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheettax
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheettradein
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealworksheetpaymentschedule_xproducts
                        WHERE worksheet_id IN
                        (
                            SELECT worksheet_id
                            FROM dealworksheet
                            WHERE inst_id = @Inst_ID
                        );
                        SELECT *
                        FROM dealaudits
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealborrowers
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM inspolicies
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealinventory
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealpaymentschedules
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealpersons
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealsends
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealtaxes
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealtransactions
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealtransplits
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealwarrantycontract
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealwarrantyrate
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealwarrantyrateoptions
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM dealworksheet
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM deals
                        WHERE inst_id = @Inst_ID;
                    END;
                --******************
                --Inventory Listings
                --******************
                IF ISNULL(@DeleteInvListings, 0) = 1
                    BEGIN
                        DELETE FEX_Summary..DTMasterSite_LotListing
                        WHERE inst_id = @Inst_ID;
                        DELETE FEX_Summary..DTMasterSite_InventoryListing
                        WHERE inst_id = @Inst_ID;
                    END;
                ELSE
                    BEGIN
                        SELECT *
                        FROM FEX_Summary..DTMasterSite_LotListing
                        WHERE inst_id = @Inst_ID;
                        SELECT *
                        FROM FEX_Summary..DTMasterSite_InventoryListing
                        WHERE inst_id = @Inst_ID;
                    END;
            END;
    END;
ELSE
	BEGIN 
		SELECT 'PLEASE ENTER YOUR DTUser_ID BEFORE RUNNING'
	END;
