USE [FEX_001]
 
	DECLARE @Transfer_ID int 
	DECLARE @Execute bit = 1 -- if this is 0 it will give a count of accounts  
	DECLARE @Inst_ID int  
	DECLARE @BranchList as varchar(250)
	
	SET @Transfer_ID = 95
	SET @Execute = 1 -- if this is 0 it will give a count of accounts  
  
select   
	  
	@Inst_ID = From_Inst_ID,  
	@BranchList = From_BranchList
from  
	FEX_TEMP..Transfer  
where  
	Transfer_ID = @Transfer_ID  
	and Status = 'O'  
  
  
		select  
			@Transfer_ID as Transfer_ID,  
			@Inst_ID as Inst_ID,  
			@BranchList as BranchList,  
			count(Acct_ID) as NumberOfInventorytoBeCopied  
		from   
			dbo.Inventory (nolock)  
			  
		where  
			Inst_ID = @Inst_ID  
			and Lot_ID in (select row from dbo.fn_DelimitedStringToRows(isnull(@BranchList,''),',')) 
			and Status not in ('S','D')
			
		SELECT * FROM FEX_TEMP..Transfer_Inventory WHERE Transfer_ID = @Transfer_ID
		SELECT * FROM FEX_TEMP..Transfer_InventoryEquipment WHERE Transfer_ID = @Transfer_ID
		SELECT * FROM FEX_TEMP..Transfer_InventoryService WHERE Transfer_ID = @Transfer_ID
		SELECT * FROM FEX_TEMP..Transfer_InventoryTitle WHERE Transfer_ID = @Transfer_ID
  
if ISNULL(@Inst_ID,0) > 0 and ISNULL(@Execute,0) = 1  
begin  
  
	update  
		FEX_TEMP..Transfer  
	set   
		Status = 'P'   
	where  
		Transfer_ID = @Transfer_ID  
		and Status = 'O'  
  
  
declare @Channel_TO_Count as int  
declare @Person_TO_Count as int  
declare @Account_TO_Count as int  
declare @Collateral_TO_Count as int  
	  
	  
	set @Channel_TO_Count = 0  
  
  

	-- acct inventory  
	print 'transfer out acct inventory'  
	insert FEX_TEMP..transfer_inventory  
		select  
		  
			@Transfer_ID,  
			0 as New_Acct_ID,  
			0 as New_Inventory_ID,  
			0 as New_Lot_ID,  
			0 as New_AcquiredChannel_ID,  
			0 as New_FinanceChannel_ID,  
			0 as New_Sale_ID,  
			0 as New_TISale_ID,  
			0 as New_RepoAcct_ID,  
			0 as New_AcquiredFrom_ID,  
			0 as New_EvaluationGroup_ID,  
			[Inventory_ID],   
			[Inst_ID],   
			[InvType_ID],   
			[StockNumber],   
			[NewFlag],   
			[Year],   
			[Make],   
			[Model],   
			[BodyStyle],   
			[ExtColor],   
			[ExtTrim],   
			[IntColor],   
			[VIN],   
			[LicPlates],   
			[LicPlatesExpire],   
			[ValNumber],   
			[InspectExpire],   
			[Mileage],   
			[MileageStatus_ID],   
			[Condition_ID],   
			[Transmission],   
			[DriveType],   
			[Engine],   
			[Comments],   
			[AskPrice],   
			[AskDown],   
			[AskTerm],   
			[ImageMain],   
			[ImageBack],   
			[ImageSide],   
			[ImageInterior],   
			[Status],   
			[Sale_ID],   
			[AcquiredPrice],   
			[AcquiredDate],   
			[AcquiredMethod],   
			[AcquiredMileage],   
			[AcquiredMileageStatus_ID],   
			[AcquiredChannel_ID],   
			[AcquiredRefNumber],   
			[PurchaseStatus],   
			[TI_Sale_ID],   
			[TI_SaleItem_ID],   
			[TI_Payoff],   
			[TI_AccountNumber],   
			[TI_CustomerName],   
			[TI_CompanyName],   
			[TI_Add1],   
			[TI_Add2],   
			[TI_City],   
			[TI_State],   
			[TI_Zip],   
			[TI_Contact],   
			[TI_Phone],   
			[TI_Fax],   
			[TI_Email],   
			[FinanceChannel_ID],   
			[FinanceDate],   
			[FinanceAmount],   
			[FinanceCloseDate],   
			[FinanceCloseRefNumber],   
			[Location],   
			[TitleLocation],   
			[ClearTitle],   
			[DescTitleProblem],   
			[DateInvTaxPaid],   
			[LastModified],   
			[AddDate],   
			[ASIS],   
			[WholesalePrice],   
			[DateToAuction],   
			[AuctionName],   
			[AuctionLane],   
			[AuctionNumber],   
			[Lot_ID],   
			[InspExpireMth],   
			[InspExpireYear],   
			[LicPlatesExpireMth],   
			[LicPlatesExpireYear],   
			[RepoAcct_ID],   
			[Buyer_ID],   
			[PakFee],   
			[ListOnWeb],   
			[Acct_ID],   
			[FinanceDueDate],   
			[InvRecoveryDevice],   
			[YearPreviousSale],   
			[Weight],   
			[Notes],   
			[WarrantyTerms],   
			[InvRecoveryDeviceType_ID],   
			[InstalledOptions],   
			[FuelType],   
			[Stereo],   
			[ListOnAutoTrader],   
			[Image1],   
			[Image2],   
			[Image3],   
			[Image4],   
			[Image5],   
			[Image6],   
			[Image7],   
			[Image8],   
			[Image9],   
			[Image10],   
			[Image11],   
			[Image12],   
			[Image13],   
			[Image14],   
			[Image15],   
			[Image16],   
			[Image17],   
			[Image18],   
			[Image19],   
			[Image20],   
			[Image21],   
			[Image22],   
			[Image23],   
			[Image24],   
			[Image25],   
			[LastDateListedWeb],   
			[LastDateListedAutoTrader],   
			[PromotionalPrice],   
			[PromotionalPayment],   
			[PromotionalTerm],   
			[SpecialFinancePrice],   
			[SpecialFinancePayment],   
			[SpecialFinanceTerm],   
			[Residual24],   
			[Residual36],   
			[Residual48],   
			[Residual60],   
			[FMV],   
			[ACV],   
			[CRV],   
			[Taxes],   
			[InitialChargeOff],   
			[OwnerDivision],   
			[AlternateLot_ID],   
			[ListOnCarsCom],   
			[LastDateListedCarsCom],   
			[AskPrice_Low],   
			[CarsComImageChanged],   
			[TempDealerTag],   
			[TempDealerTagIssueDate],   
			[TempSalesTag],   
			[TempSalesTagIssueDate],   
			[TempSalesExtTag],   
			[TempSalesExtTagIssueDate],   
			[OnTimeContract_ID],   
			[TitleNumber],   
			[EmmissionsNumber],   
			[InvCreationMethod_ID],   
			[SellerNotes],   
			[ExtColorCommon],   
			[ImageCount],   
			[InternetSpecial],   
			[VROXCertificate],   
			[Inspected],   
			[InvRecoverySubDeviceType_ID],   
			[CertCode],   
			[CertVendor_ID],   
			[CertStatus],   
			[CertDate],   
			[DeletedDate],   
			[AcquiredFromType_ID],   
			[AcquiredFrom_ID],   
			[TotalExpenses],   
			[EvaluationGroup_ID],   
			[EvaluationDate],   
			[EvaluationOfferPrice],   
			[EvaluationBidPrice],   
			[EvaluationBidExpiration],   
			[EvaluationLane],   
			[EvaluationNumber],   
			[ExtPrimaryCommonColor],   
			[ExtSecondaryCommonColor],   
			[AutoCheck_Requested],
			InvRecoveryDeviceExpDate,
			LastStatus,
			SiriusStatus_ID,
			ToLocationDate,
			StatusChangeDate  
	from   
		inventory (nolock)  
	where  
		Inst_ID = @Inst_ID  
		AND Lot_ID in (select row from dbo.fn_DelimitedStringToRows(isnull(@BranchList,''),','))
		AND Status not in ('S','D') 
	select @Collateral_TO_Count = @@ROWCOUNT		  
	-- acquiredchannels  
	print 'transfer out acquired channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country   
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				acquiredchannel_id  
			from   
				fex_temp..transfer_inventory (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
	  
	-- acquiredchannels  
	print 'transfer out finance channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country  
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				FinanceChannel_ID  
			from   
				fex_temp..transfer_inventory (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
  
	-- accounts inventoryequipment  
	print 'transfer out inventoryequipment'  
	insert FEX_TEMP..transfer_inventoryequipment  
		select  
			@Transfer_ID,  
			0 as New_Inventory_ID,  
			[Inst_ID],   
			[Inventory_ID],   
			[Equipment_Id],   
			[CustomDesc]  
	from   
		inventoryequipment (nolock)  
	where  
		Inst_ID = @Inst_ID   
		and inventory_ID in  
		(  
		select  
			Inventory_ID  
		from   
			fex_temp..transfer_inventory (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		)  
	  
  
	-- inventoryservice  
	print 'transfer out inventoryservice'  
	insert FEX_TEMP..transfer_inventoryservice  
		select  
			@Transfer_ID,  
			0 as New_Inventory_ID,  
			0 as New_ServiceComp_ID,  
			[InvService_ID],   
			[Inst_ID],   
			[Inventory_ID],   
			[ServiceComp_ID],   
			[ServiceDate],   
			[PostDate],   
			[PostedBy],   
			[Estimate],   
			[FinalCost],   
			[Status],   
			[RefNumber],   
			[Payment],   
			[PayRefNumber],   
			[Paid],   
			[PaidDate],   
			[PaidBy],   
			[Description],   
			[ExportGUID],   
			[ClassCode],   
			[DTUser_ID],  
			isnull(deleted,0),
			PaymentForm_ID  
	from   
		inventoryservice (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and inventory_ID in  
		(  
		select  
			Inventory_ID  
		from   
			fex_temp..transfer_inventory (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		)  
	  
	  
		-- acquiredchannels  
	print 'transfer out service company channels'  
	insert FEX_TEMP..Transfer_Channels  
		select  
			@Transfer_ID,  
			0 as New_Channel_ID,  
			[Inst_Id],   
			[Channel_ID],   
			[ChannelType_ID],   
			[ChannelDescription],   
			[Name],   
			[Add1],   
			[Add2],   
			[City],   
			[County],   
			[State],   
			[Zip],   
			[Contact],   
			[Phone1],   
			[Phone2],   
			[Fax1],   
			[Fax2],   
			[Email],   
			[WebSite],   
			[FedID],   
			[SalesTaxID],   
			[Status],   
			[AssignedDealerCode],   
			[AssignedBranchCode],   
			[AssignedInstCode],   
			[AssignedProductCode],   
			[Pipeline],   
			[PipelineUserId],   
			[PipelinePW],   
			[DefSendMethod],   
			[ElectronicFormat],   
			[AutoSend],   
			[IsInstChannel],   
			[IsDTSUser],   
			[ChannelUserCode],   
			[ChannelPassword],   
			[AddDate],   
			[LastModified],   
			[AssociatedPool_ID],   
			[InHouseServicing],   
			[CashPool_ID],   
			[DealerPool_ID],   
			[GDN],   
			[VITDistrictName],   
			[VITDistrictAdd1],   
			[VITDistrictAdd2],   
			[VITDistrictCity],   
			[VITDistrictState],   
			[VITDistrictZip],   
			[VITAccountNumber],   
			[VITDistrictPhone1],   
			[VITCollectorName],   
			[VITCollectorAdd1],   
			[VITCollectorAdd2],   
			[VITCollectorCity],   
			[VITCollectorState],   
			[VITCollectorZip],   
			[VITCollectorPhone1],   
			[TaxPayerName],   
			[TaxPayerAdd1],   
			[TaxPayerAdd2],   
			[TaxPayerCity],   
			[TaxPayerState],   
			[TaxPayerCounty],   
			[TaxPayerZip],   
			[TaxPayerPhone1],   
			[TaxPayerFax1],   
			[DefFinanceContract_ID],   
			[RFC],   
			[Company_ID],   
			[DiscountMethod_ID],   
			[InsureExpressCode],   
			[Vendor_ID],   
			[PassTimeDealerAcctNum],   
			[PassTimeWarningDays],   
			[PassTimeEmergencyDays],   
			[OnTimeSiteID],   
			[OnTimeGraceDays],   
			[OnTimeShutOffTime],   
			[OnTimeUserCode],   
			[OnTimeUserPass],   
			[FEXLender_ID],   
			[VITAgency_ID],   
			[SalesTaxAgency_ID],   
			[LocationCode],   
			[EmployeeCode],   
			[FI_Role],   
			[Manager_Role],   
			[Sales_Role],   
			[IsSystem],
			AccountingVersion,
			AccountingClass,
			GLAcctNumber,
			TextName,
			IntegrationType,
			Country   
	from   
		channels (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and Channel_ID in  
		(  
			select  
				ServiceComp_ID  
			from   
				fex_temp..transfer_inventoryservice (nolock)  
			where  
				Inst_ID = @Inst_ID  
				and Transfer_ID = @Transfer_ID  
			)  
		and Channel_ID not in  
			(select  
					channel_id  
				from   
					fex_temp..transfer_channels (nolock)  
				where  
					Inst_ID = @Inst_ID  
					and Transfer_ID = @Transfer_ID  
			)  
  
	select @Channel_TO_Count = @Channel_TO_Count + @@ROWCOUNT  
	  
	-- inventorytitle  
	print 'transfer out inventorytitle'  
	insert FEX_TEMP..transfer_inventorytitle  
		select  
			@Transfer_ID,  
			0 as New_Inventory_ID,  
			0 as New_TitleLocation_ID,  
			[Title_ID],   
			[Inst_ID],   
			[Inventory_ID],   
			[TitleStatus_ID],   
			[TitleComments],   
			[TitleLocation_ID],   
			[TitleNumber],   
			[COTName],   
			[COTAdd1],   
			[COTAdd2],   
			[COTCity],   
			[COTCounty],   
			[COTState],   
			[COTZip],   
			[COTCountry],   
			[COTPhone1],   
			[COTFax1],   
			[LienAcct_ID],   
			[LienBalance],   
			[LienName],   
			[LienAcctNumber],   
			[LienContact],   
			[LienAdd1],   
			[LienAdd2],   
			[LienCity],   
			[LienCounty],   
			[LienState],   
			[LienZip],   
			[LienCountry],   
			[LienPhone1],   
			[LienFax1],   
			[LastModified],   
			[AddDate],   
			[AltTitleLocation],
			VendorChannel_ID,
			PrevTitleTransferred,
			PrevTitleSent,
			PrevTitleNumber,
			PrevTitleState,
			PrevPostalCode,
			PrevCity,
			PrevState,
			PrevCounty,
			PrevTitleReceived  
	from   
		inventorytitle (nolock)  
	where  
		Inst_ID = @Inst_ID  
		and inventory_ID in  
		(  
		select  
			Inventory_ID  
		from   
			fex_temp..transfer_inventory (nolock)  
		where  
			Inst_ID = @Inst_ID  
			and Transfer_ID = @Transfer_ID  
		) 		  
			  
	update   
		FEX_TEMP..Transfer  
	set  
		Status = 'TI', 
		Collateral_TO_Count = @Collateral_TO_Count  
	where  
		Transfer_ID = @Transfer_ID  
		  
			  
end  
		  
		  
  
  
    
  
  
  
  
  
  
  
  
  
