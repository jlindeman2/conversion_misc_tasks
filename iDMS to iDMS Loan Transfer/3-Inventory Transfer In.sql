USE [FEX_001]

DECLARE	@Transfer_ID int
DECLARE	@From_Inst_ID int
DECLARE	@To_Inst_ID int
DECLARE	@New_Lot_ID INT

SET @Transfer_ID = 153

select   	  
	@From_Inst_ID = From_Inst_ID,
	@To_Inst_ID = To_Inst_ID,  
	@New_Lot_ID  = To_Branch_ID
from  
	FEX_TEMP..Transfer  
where  
	Transfer_ID = @Transfer_ID

	declare @SuccessCount int
	declare @CollateralTransferStatus as varchar(2)
	
	set @CollateralTransferStatus = ''
	set @SuccessCount = 0

	
	update
		FEX_TEMP..Transfer
	set
		Collateral_Status = '',
		@SuccessCount = collateral_TI_count
	where
		Transfer_ID = @Transfer_ID
		
	set @SuccessCount = isnull(@SuccessCount,0)
		
	/*
	#############################################################
	account balance transfer
	#############################################################
	*/
	
		-- accounts
	--print 'transfer In account collateral'
	declare @New_Inventory_ID as int
	declare @Old_Inventory_ID as int
	declare @Cur_Inventory_ID as int
	declare @bStop_Inventory as bit
	declare @ErrMessage as varchar(50)
	
	
	declare @New_Sale_ID as int
	declare @New_TISale_ID as int
	declare @New_RepoAcct_ID as int
	declare @New_AcquiredChannel_ID as int
	declare @New_FinanceChannel_ID as int
	declare @New_AcquiredFrom_ID as int
	declare @New_EvaluationGroup_ID as int
	
	
	--declare @Inventory_ID as int
	--declare @Inst_ID as int
	declare @InvType_ID as int
	declare @StockNumber as varchar(50) 
	declare @NewFlag as bit
	declare @Year as smallint
	declare @Make as varchar(30) 
	declare @Model as varchar(30) 
	declare @BodyStyle as varchar(50) 
	declare @ExtColor as varchar(75) 
	declare @ExtTrim as varchar(75) 
	declare @IntColor as varchar(75) 
	declare @VIN as varchar(20) 
	declare @LicPlates as varchar(10) 
	declare @LicPlatesExpire as datetime
	declare @ValNumber as varchar(50) 
	declare @InspectExpire as datetime
	declare @Mileage as int
	declare @MileageStatus_ID as int
	declare @Condition_ID as int
	declare @Transmission as varchar(50) 
	declare @DriveType as varchar(50) 
	declare @Engine as varchar(50) 
	declare @Comments as varchar(2000) 
	declare @AskPrice as money
	declare @AskDown as money
	declare @AskTerm as int
	declare @ImageMain as varchar(50) 
	declare @ImageBack as varchar(50) 
	declare @ImageSide as varchar(50) 
	declare @ImageInterior as varchar(50) 
	declare @Status as char(1) 
	--declare @Sale_ID as int
	declare @AcquiredPrice as money
	declare @AcquiredDate as datetime
	declare @AcquiredMethod as char(1) 
	declare @AcquiredMileage as int
	declare @AcquiredMileageStatus_ID as int
	--declare @AcquiredChannel_ID as int
	declare @AcquiredRefNumber as varchar(50) 
	declare @PurchaseStatus as char(1) 
	--declare @TI_Sale_ID as int
	--declare @TI_SaleItem_ID as int
	declare @TI_Payoff as money
	declare @TI_AccountNumber as varchar(25) 
	declare @TI_CustomerName as varchar(50) 
	declare @TI_CompanyName as varchar(50) 
	declare @TI_Add1 as varchar(50) 
	declare @TI_Add2 as varchar(50) 
	declare @TI_City as varchar(30) 
	declare @TI_State as char(2) 
	declare @TI_Zip as varchar(10) 
	declare @TI_Contact as varchar(50) 
	declare @TI_Phone as varchar(15) 
	declare @TI_Fax as varchar(15) 
	declare @TI_Email as varchar(80) 
	--declare @FinanceChannel_ID as int
	declare @FinanceDate as datetime
	declare @FinanceAmount as money
	declare @FinanceCloseDate as datetime
	declare @FinanceCloseRefNumber as varchar(50) 
	declare @Location as varchar(50) 
	declare @TitleLocation as varchar(50) 
	declare @ClearTitle as bit
	declare @DescTitleProblem as varchar(50) 
	declare @DateInvTaxPaid as datetime
	declare @LastModified as datetime
	declare @AddDate as datetime
	declare @ASIS as bit
	declare @WholesalePrice as money
	declare @DateToAuction as datetime
	declare @AuctionName as varchar(80) 
	declare @AuctionLane as varchar(50) 
	declare @AuctionNumber as varchar(50) 
	--declare @Lot_ID as int
	declare @InspExpireMth as int
	declare @InspExpireYear as int
	declare @LicPlatesExpireMth as int
	declare @LicPlatesExpireYear as int
	--declare @RepoAcct_ID as int
	--declare @Buyer_ID as int
	declare @PakFee as money
	declare @ListOnWeb as bit
	--declare @Acct_ID as int
	declare @FinanceDueDate as datetime
	declare @InvRecoveryDevice as varchar(100) 
	declare @YearPreviousSale as smallint
	declare @Weight as int
	declare @Notes as varchar(500) 
	declare @WarrantyTerms as varchar(100) 
	declare @InvRecoveryDeviceType_ID as int
	declare @InstalledOptions as varchar(500) 
	declare @FuelType as varchar(50) 
	declare @Stereo as varchar(50) 
	declare @ListOnAutoTrader as bit
	declare @Image1 as varchar(50) 
	declare @Image2 as varchar(50) 
	declare @Image3 as varchar(50) 
	declare @Image4 as varchar(50) 
	declare @Image5 as varchar(50) 
	declare @Image6 as varchar(50) 
	declare @Image7 as varchar(50) 
	declare @Image8 as varchar(50) 
	declare @Image9 as varchar(50) 
	declare @Image10 as varchar(50) 
	declare @Image11 as varchar(50) 
	declare @Image12 as varchar(50) 
	declare @Image13 as varchar(50) 
	declare @Image14 as varchar(50) 
	declare @Image15 as varchar(50) 
	declare @Image16 as varchar(50) 
	declare @Image17 as varchar(50) 
	declare @Image18 as varchar(50) 
	declare @Image19 as varchar(50) 
	declare @Image20 as varchar(50) 
	declare @Image21 as varchar(50) 
	declare @Image22 as varchar(50) 
	declare @Image23 as varchar(50) 
	declare @Image24 as varchar(50) 
	declare @Image25 as varchar(50) 
	declare @LastDateListedWeb as datetime
	declare @LastDateListedAutoTrader as datetime
	declare @PromotionalPrice as money
	declare @PromotionalPayment as money
	declare @PromotionalTerm as int
	declare @SpecialFinancePrice as money
	declare @SpecialFinancePayment as money
	declare @SpecialFinanceTerm as int
	declare @Residual24 as money
	declare @Residual36 as money
	declare @Residual48 as money
	declare @Residual60 as money
	declare @FMV as money
	declare @ACV as money
	declare @CRV as money
	declare @Taxes as money
	declare @InitialChargeOff as money
	declare @OwnerDivision as varchar(50) 
--	declare @AlternateLot_ID as int
	declare @ListOnCarsCom as bit
	declare @LastDateListedCarsCom as datetime
	declare @AskPrice_Low as money
	declare @CarsComImageChanged as bit
	declare @TempDealerTag as varchar(50) 
	declare @TempDealerTagIssueDate as datetime
	declare @TempSalesTag as varchar(50) 
	declare @TempSalesTagIssueDate as datetime
	declare @TempSalesExtTag as varchar(50) 
	declare @TempSalesExtTagIssueDate as datetime
	declare @OnTimeContract_ID as int
	declare @TitleNumber as varchar(50) 
	declare @EmmissionsNumber as varchar(50) 
	declare @InvCreationMethod_ID as int
	declare @SellerNotes as varchar(2000) 
	declare @ExtColorCommon as varchar(20) 
	declare @ImageCount as int
	declare @InternetSpecial as bit
	declare @VROXCertificate as varchar(200) 
	declare @Inspected as bit
	declare @InvRecoverySubDeviceType_ID as int
	declare @CertCode as varchar(50) 
	declare @CertVendor_ID as int
	declare @CertStatus as varchar(2) 
	declare @CertDate as datetime
	declare @DeletedDate as datetime
	declare @AcquiredFromType_ID as int
	--declare @AcquiredFrom_ID as int
	declare @TotalExpenses as money
	declare @EvaluationGroup_ID as int
	declare @EvaluationDate as datetime
	declare @EvaluationOfferPrice as money
	declare @EvaluationBidPrice as money
	declare @EvaluationBidExpiration as datetime
	declare @EvaluationLane as varchar(50) 
	declare @EvaluationNumber as varchar(50) 
	declare @ExtPrimaryCommonColor as varchar(50) 
	declare @ExtSecondaryCommonColor as varchar(50) 
	declare @AutoCheck_Requested as BIT
    DECLARE @InvRecoveryDeviceExpDate AS DATETIME
	DECLARE @LastStatus as CHAR(1)
	
	
	
	
	set @bStop_Inventory = 0	
	set @Cur_Inventory_ID = 0
	--set @Old_Acct_ID = 0
		
	while @bStop_Inventory = 0
	begin
	
		set @ErrMessage = ''
		set @New_Inventory_ID = 0
		set @Old_Inventory_ID = 0
		
		select 
			top 1
				@Old_Inventory_ID = inv.Inventory_ID,
				--@Inventory_ID = Inventory_ID,
				--@Inst_ID = inv.Inst_ID,
				@InvType_ID = inv.InvType_ID,
				@StockNumber = inv.StockNumber,
				@NewFlag = inv.NewFlag,
				@Year = inv.Year,
				@Make = inv.Make,
				@Model = inv.Model,
				@BodyStyle = inv.BodyStyle,
				@ExtColor = inv.ExtColor,
				@ExtTrim = inv.ExtTrim,
				@IntColor = inv.IntColor,
				@VIN = inv.VIN,
				@LicPlates = inv.LicPlates,
				@LicPlatesExpire = inv.LicPlatesExpire,
				@ValNumber = inv.ValNumber,
				@InspectExpire = inv.InspectExpire,
				@Mileage = inv.Mileage,
				@MileageStatus_ID = inv.MileageStatus_ID,
				@Condition_ID = inv.Condition_ID,
				@Transmission = inv.Transmission,
				@DriveType = inv.DriveType,
				@Engine = inv.Engine,
				@Comments = inv.Comments,
				@AskPrice = inv.AskPrice,
				@AskDown = inv.AskDown,
				@AskTerm = inv.AskTerm,
				@ImageMain = inv.ImageMain,
				@ImageBack = inv.ImageBack,
				@ImageSide = inv.ImageSide,
				@ImageInterior = inv.ImageInterior,
				@Status = inv.Status,
				@New_Sale_ID = inv.New_Sale_ID,
				@AcquiredPrice = inv.AcquiredPrice,
				@AcquiredDate = inv.AcquiredDate,
				@AcquiredMethod = inv.AcquiredMethod,
				@AcquiredMileage = inv.AcquiredMileage,
				@AcquiredMileageStatus_ID = inv.AcquiredMileageStatus_ID,
				@New_AcquiredChannel_ID = inv.New_AcquiredChannel_ID,
				@AcquiredRefNumber = inv.AcquiredRefNumber,
				@PurchaseStatus = inv.PurchaseStatus,
				@New_TISale_ID = inv.New_TISale_ID,
				--@New_TI_SaleItem_ID = 0, --inv.TI_SaleItem_ID,
				@TI_Payoff = inv.TI_Payoff,
				@TI_AccountNumber = inv.TI_AccountNumber,
				@TI_CustomerName = inv.TI_CustomerName,
				@TI_CompanyName = inv.TI_CompanyName,
				@TI_Add1 = inv.TI_Add1,
				@TI_Add2 = inv.TI_Add2,
				@TI_City = inv.TI_City,
				@TI_State = inv.TI_State,
				@TI_Zip = inv.TI_Zip,
				@TI_Contact = inv.TI_Contact,
				@TI_Phone = inv.TI_Phone,
				@TI_Fax = inv.TI_Fax,
				@TI_Email = inv.TI_Email,
				@New_FinanceChannel_ID = inv.New_FinanceChannel_ID,
				@FinanceDate = inv.FinanceDate,
				@FinanceAmount = inv.FinanceAmount,
				@FinanceCloseDate = inv.FinanceCloseDate,
				@FinanceCloseRefNumber = inv.FinanceCloseRefNumber,
				@Location = inv.Location,
				@TitleLocation = inv.TitleLocation,
				@ClearTitle = inv.ClearTitle,
				@DescTitleProblem = inv.DescTitleProblem,
				@DateInvTaxPaid = inv.DateInvTaxPaid,
				@LastModified = inv.LastModified,
				@AddDate = inv.AddDate,
				@ASIS = inv.ASIS,
				@WholesalePrice = inv.WholesalePrice,
				@DateToAuction = inv.DateToAuction,
				@AuctionName = inv.AuctionName,
				@AuctionLane = inv.AuctionLane,
				@AuctionNumber = inv.AuctionNumber,
				--@Lot_ID = inv.Lot_ID,
				@InspExpireMth = inv.InspExpireMth,
				@InspExpireYear = inv.InspExpireYear,
				@LicPlatesExpireMth = inv.LicPlatesExpireMth,
				@LicPlatesExpireYear = inv.LicPlatesExpireYear,
				@New_RepoAcct_ID = inv.New_RepoAcct_ID,
				--@New_Buyer_ID = inv.New_Buyer_ID,
				@PakFee = inv.PakFee,
				@ListOnWeb = inv.ListOnWeb,
				--@Acct_ID = inv.New_Acct_ID,
				@FinanceDueDate = inv.FinanceDueDate,
				@InvRecoveryDevice = inv.InvRecoveryDevice,
				@YearPreviousSale = inv.YearPreviousSale,
				@Weight = inv.Weight,
				@Notes = inv.Notes,
				@WarrantyTerms = inv.WarrantyTerms,
				@InvRecoveryDeviceType_ID = inv.InvRecoveryDeviceType_ID,
				@InstalledOptions = inv.InstalledOptions,
				@FuelType = inv.FuelType,
				@Stereo = inv.Stereo,
				@ListOnAutoTrader = inv.ListOnAutoTrader,
				@Image1 = inv.Image1,
				@Image2 = inv.Image2,
				@Image3 = inv.Image3,
				@Image4 = inv.Image4,
				@Image5 = inv.Image5,
				@Image6 = inv.Image6,
				@Image7 = inv.Image7,
				@Image8 = inv.Image8,
				@Image9 = inv.Image9,
				@Image10 = inv.Image10,
				@Image11 = inv.Image11,
				@Image12 = inv.Image12,
				@Image13 = inv.Image13,
				@Image14 = inv.Image14,
				@Image15 = inv.Image15,
				@Image16 = inv.Image16,
				@Image17 = inv.Image17,
				@Image18 = inv.Image18,
				@Image19 = inv.Image19,
				@Image20 = inv.Image20,
				@Image21 = inv.Image21,
				@Image22 = inv.Image22,
				@Image23 = inv.Image23,
				@Image24 = inv.Image24,
				@Image25 = inv.Image25,
				@LastDateListedWeb = inv.LastDateListedWeb,
				@LastDateListedAutoTrader = inv.LastDateListedAutoTrader,
				@PromotionalPrice = inv.PromotionalPrice,
				@PromotionalPayment = inv.PromotionalPayment,
				@PromotionalTerm = inv.PromotionalTerm,
				@SpecialFinancePrice = inv.SpecialFinancePrice,
				@SpecialFinancePayment = inv.SpecialFinancePayment,
				@SpecialFinanceTerm = inv.SpecialFinanceTerm,
				@Residual24 = inv.Residual24,
				@Residual36 = inv.Residual36,
				@Residual48 = inv.Residual48,
				@Residual60 = inv.Residual60,
				@FMV = inv.FMV,
				@ACV = inv.ACV,
				@CRV = inv.CRV,
				@Taxes = inv.Taxes,
				@InitialChargeOff = inv.InitialChargeOff,
				@OwnerDivision = inv.OwnerDivision,
				--@AlternateLot_ID = 0, --inv.AlternateLot_ID,
				@ListOnCarsCom = inv.ListOnCarsCom,
				@LastDateListedCarsCom = inv.LastDateListedCarsCom,
				@AskPrice_Low = inv.AskPrice_Low,
				@CarsComImageChanged = inv.CarsComImageChanged,
				@TempDealerTag = inv.TempDealerTag,
				@TempDealerTagIssueDate = inv.TempDealerTagIssueDate,
				@TempSalesTag = inv.TempSalesTag,
				@TempSalesTagIssueDate = inv.TempSalesTagIssueDate,
				@TempSalesExtTag = inv.TempSalesExtTag,
				@TempSalesExtTagIssueDate = inv.TempSalesExtTagIssueDate,
				@OnTimeContract_ID = inv.OnTimeContract_ID,
				@TitleNumber = inv.TitleNumber,
				@EmmissionsNumber = inv.EmmissionsNumber,
				@InvCreationMethod_ID = inv.InvCreationMethod_ID,
				@SellerNotes = inv.SellerNotes,
				@ExtColorCommon = inv.ExtColorCommon,
				@ImageCount = inv.ImageCount,
				@InternetSpecial = inv.InternetSpecial,
				@VROXCertificate = inv.VROXCertificate,
				@Inspected = inv.Inspected,
				@InvRecoverySubDeviceType_ID = inv.InvRecoverySubDeviceType_ID,
				@CertCode = inv.CertCode,
				@CertVendor_ID = inv.CertVendor_ID,
				@CertStatus = inv.CertStatus,
				@CertDate = inv.CertDate,
				@DeletedDate = inv.DeletedDate,
				@AcquiredFromType_ID = inv.AcquiredFromType_ID,
				@New_AcquiredFrom_ID = inv.New_AcquiredFrom_ID,
				@TotalExpenses = inv.TotalExpenses,
				@EvaluationGroup_ID = 0, --inv.EvaluationGroup_ID,
				@EvaluationDate = null, --inv.EvaluationDate,
				@EvaluationOfferPrice = 0, --inv.EvaluationOfferPrice,
				@EvaluationBidPrice = 0, --inv.EvaluationBidPrice,
				@EvaluationBidExpiration = null, --inv.EvaluationBidExpiration,
				@EvaluationLane = '', --inv.EvaluationLane,
				@EvaluationNumber = '', --inv.EvaluationNumber,
				@ExtPrimaryCommonColor = inv.ExtPrimaryCommonColor,
				@ExtSecondaryCommonColor = inv.ExtSecondaryCommonColor,
				@AutoCheck_Requested = 0, --inv.AutoCheck_Requested
				@InvRecoveryDeviceExpDate = '', --inv.InvRecoveryDeviceExpDate,
				@LastStatus = '' --inv.LastStatus
		from
			fex_temp..Transfer_Inventory inv (nolock)
		where
			Transfer_ID = @Transfer_ID
			and Inst_ID = @From_Inst_ID
			and isnull(New_Inventory_ID,0) = 0
			and Inventory_ID > @Cur_Inventory_ID
		order by 
			Inst_ID,Acct_ID,Inventory_ID	
			
		if ISNULL(@Old_Inventory_ID,0) = 0
		begin
			set @bStop_Inventory = 1
		end	
		else
		begin
			set @Cur_Inventory_ID = @Old_Inventory_ID
			
			BEGIN TRY
			
				insert
					inventory
				(	
					[Inst_ID], 
					[InvType_ID], 
					[StockNumber], 
					[NewFlag], 
					[Year], 
					[Make], 
					[Model], 
					[BodyStyle], 
					[ExtColor], 
					[ExtTrim], 
					[IntColor], 
					[VIN], 
					[LicPlates], 
					[LicPlatesExpire], 
					[ValNumber], 
					[InspectExpire], 
					[Mileage], 
					[MileageStatus_ID], 
					[Condition_ID], 
					[Transmission], 
					[DriveType], 
					[Engine], 
					[Comments], 
					[AskPrice], 
					[AskDown], 
					[AskTerm], 
					[ImageMain], 
					[ImageBack], 
					[ImageSide], 
					[ImageInterior], 
					[Status], 
					[Sale_ID], 
					[AcquiredPrice], 
					[AcquiredDate], 
					[AcquiredMethod], 
					[AcquiredMileage], 
					[AcquiredMileageStatus_ID], 
					[AcquiredChannel_ID], 
					[AcquiredRefNumber], 
					[PurchaseStatus], 
					[TI_Sale_ID], 
					[TI_SaleItem_ID], 
					[TI_Payoff], 
					[TI_AccountNumber], 
					[TI_CustomerName], 
					[TI_CompanyName], 
					[TI_Add1], 
					[TI_Add2], 
					[TI_City], 
					[TI_State], 
					[TI_Zip], 
					[TI_Contact], 
					[TI_Phone], 
					[TI_Fax], 
					[TI_Email], 
					[FinanceChannel_ID], 
					[FinanceDate], 
					[FinanceAmount], 
					[FinanceCloseDate], 
					[FinanceCloseRefNumber], 
					[Location], 
					[TitleLocation], 
					[ClearTitle], 
					[DescTitleProblem], 
					[DateInvTaxPaid], 
					[LastModified], 
					[AddDate], 
					[ASIS], 
					[WholesalePrice], 
					[DateToAuction], 
					[AuctionName], 
					[AuctionLane], 
					[AuctionNumber], 
					[Lot_ID], 
					[InspExpireMth], 
					[InspExpireYear], 
					[LicPlatesExpireMth], 
					[LicPlatesExpireYear], 
					[RepoAcct_ID], 
					[Buyer_ID], 
					[PakFee], 
					[ListOnWeb], 
					[Acct_ID], 
					[FinanceDueDate], 
					[InvRecoveryDevice], 
					[YearPreviousSale], 
					[Weight], 
					[Notes], 
					[WarrantyTerms], 
					[InvRecoveryDeviceType_ID], 
					[InstalledOptions], 
					[FuelType], 
					[Stereo], 
					[ListOnAutoTrader], 
					[Image1], 
					[Image2], 
					[Image3], 
					[Image4], 
					[Image5], 
					[Image6], 
					[Image7], 
					[Image8], 
					[Image9], 
					[Image10], 
					[Image11], 
					[Image12], 
					[Image13], 
					[Image14], 
					[Image15], 
					[Image16], 
					[Image17], 
					[Image18], 
					[Image19], 
					[Image20], 
					[Image21], 
					[Image22], 
					[Image23], 
					[Image24], 
					[Image25], 
					[LastDateListedWeb], 
					[LastDateListedAutoTrader], 
					[PromotionalPrice], 
					[PromotionalPayment], 
					[PromotionalTerm], 
					[SpecialFinancePrice], 
					[SpecialFinancePayment], 
					[SpecialFinanceTerm], 
					[Residual24], 
					[Residual36], 
					[Residual48], 
					[Residual60], 
					[FMV], 
					[ACV], 
					[CRV], 
					[Taxes], 
					[InitialChargeOff], 
					[OwnerDivision], 
					[AlternateLot_ID], 
					[ListOnCarsCom], 
					[LastDateListedCarsCom], 
					[AskPrice_Low], 
					[CarsComImageChanged], 
					[TempDealerTag], 
					[TempDealerTagIssueDate], 
					[TempSalesTag], 
					[TempSalesTagIssueDate], 
					[TempSalesExtTag], 
					[TempSalesExtTagIssueDate], 
					[OnTimeContract_ID], 
					[TitleNumber], 
					[EmmissionsNumber], 
					[InvCreationMethod_ID], 
					[SellerNotes], 
					[ExtColorCommon], 
					[ImageCount], 
					[InternetSpecial], 
					[VROXCertificate], 
					[Inspected], 
					[InvRecoverySubDeviceType_ID], 
					[CertCode], 
					[CertVendor_ID], 
					[CertStatus], 
					[CertDate], 
					[DeletedDate], 
					[AcquiredFromType_ID], 
					[AcquiredFrom_ID], 
					[TotalExpenses], 
					[EvaluationGroup_ID], 
					[EvaluationDate], 
					[EvaluationOfferPrice], 
					[EvaluationBidPrice], 
					[EvaluationBidExpiration], 
					[EvaluationLane], 
					[EvaluationNumber], 
					[ExtPrimaryCommonColor], 
					[ExtSecondaryCommonColor], 
					[AutoCheck_Requested],
					[InvRecoveryDeviceExpDate],
					[LastStatus]
				)values(
					@To_Inst_ID, 
					@InvType_ID, 
					@StockNumber, 
					@NewFlag, 
					@Year, 
					@Make, 
					@Model, 
					@BodyStyle, 
					@ExtColor, 
					@ExtTrim, 
					@IntColor, 
					@VIN, 
					@LicPlates, 
					@LicPlatesExpire, 
					@ValNumber, 
					@InspectExpire, 
					@Mileage, 
					@MileageStatus_ID, 
					@Condition_ID, 
					@Transmission, 
					@DriveType, 
					@Engine, 
					@Comments, 
					@AskPrice, 
					@AskDown, 
					@AskTerm, 
					@ImageMain, 
					@ImageBack, 
					@ImageSide, 
					@ImageInterior, 
					@Status, 
					@New_Sale_ID, 
					@AcquiredPrice, 
					@AcquiredDate, 
					@AcquiredMethod, 
					@AcquiredMileage, 
					@AcquiredMileageStatus_ID, 
					@New_AcquiredChannel_ID, 
					@AcquiredRefNumber, 
					@PurchaseStatus, 
					@New_TISale_ID, 
					0, --@TI_SaleItem_ID, 
					@TI_Payoff, 
					@TI_AccountNumber, 
					@TI_CustomerName, 
					@TI_CompanyName, 
					@TI_Add1, 
					@TI_Add2, 
					@TI_City, 
					@TI_State, 
					@TI_Zip, 
					@TI_Contact, 
					@TI_Phone, 
					@TI_Fax, 
					@TI_Email, 
					@New_FinanceChannel_ID, 
					@FinanceDate, 
					@FinanceAmount, 
					@FinanceCloseDate, 
					@FinanceCloseRefNumber, 
					@Location, 
					@TitleLocation, 
					@ClearTitle, 
					@DescTitleProblem, 
					@DateInvTaxPaid, 
					@LastModified, 
					@AddDate, 
					@ASIS, 
					@WholesalePrice, 
					@DateToAuction, 
					@AuctionName, 
					@AuctionLane, 
					@AuctionNumber, 
					@New_Lot_ID, 
					@InspExpireMth, 
					@InspExpireYear, 
					@LicPlatesExpireMth, 
					@LicPlatesExpireYear, 
					@New_RepoAcct_ID, 
					0, ---@New_Buyer_ID, 
					@PakFee, 
					@ListOnWeb, 
					0, 
					@FinanceDueDate, 
					@InvRecoveryDevice, 
					@YearPreviousSale, 
					@Weight, 
					@Notes, 
					@WarrantyTerms, 
					@InvRecoveryDeviceType_ID, 
					@InstalledOptions, 
					@FuelType, 
					@Stereo, 
					@ListOnAutoTrader, 
					@Image1, 
					@Image2, 
					@Image3, 
					@Image4, 
					@Image5, 
					@Image6, 
					@Image7, 
					@Image8, 
					@Image9, 
					@Image10, 
					@Image11, 
					@Image12, 
					@Image13, 
					@Image14, 
					@Image15, 
					@Image16, 
					@Image17, 
					@Image18, 
					@Image19, 
					@Image20, 
					@Image21, 
					@Image22, 
					@Image23, 
					@Image24, 
					@Image25, 
					@LastDateListedWeb, 
					@LastDateListedAutoTrader, 
					@PromotionalPrice, 
					@PromotionalPayment, 
					@PromotionalTerm, 
					@SpecialFinancePrice, 
					@SpecialFinancePayment, 
					@SpecialFinanceTerm, 
					@Residual24, 
					@Residual36, 
					@Residual48, 
					@Residual60, 
					@FMV, 
					@ACV, 
					@CRV, 
					@Taxes, 
					@InitialChargeOff, 
					@OwnerDivision, 
					0, --@AlternateLot_ID, 
					@ListOnCarsCom, 
					@LastDateListedCarsCom, 
					@AskPrice_Low, 
					@CarsComImageChanged, 
					@TempDealerTag, 
					@TempDealerTagIssueDate, 
					@TempSalesTag, 
					@TempSalesTagIssueDate, 
					@TempSalesExtTag, 
					@TempSalesExtTagIssueDate, 
					@OnTimeContract_ID, 
					@TitleNumber, 
					@EmmissionsNumber, 
					@InvCreationMethod_ID, 
					@SellerNotes, 
					@ExtColorCommon, 
					@ImageCount, 
					@InternetSpecial, 
					@VROXCertificate, 
					0, -- cannot be null @Inspected, 
					@InvRecoverySubDeviceType_ID, 
					@CertCode, 
					@CertVendor_ID, 
					@CertStatus, 
					@CertDate, 
					@DeletedDate, 
					@AcquiredFromType_ID, 
					@New_AcquiredFrom_ID, 
					isnull(@TotalExpenses,0), 
					@EvaluationGroup_ID, 
					@EvaluationDate, 
					@EvaluationOfferPrice, 
					@EvaluationBidPrice, 
					@EvaluationBidExpiration, 
					@EvaluationLane, 
					@EvaluationNumber, 
					@ExtPrimaryCommonColor, 
					@ExtSecondaryCommonColor, 
					@AutoCheck_Requested,
					@InvRecoveryDeviceExpDate,
					@LastStatus
				)
				-- get new idenity
				select @New_Inventory_ID = @@IDENTITY
				
		
			
				if ISNULL(@New_Inventory_ID,0) > 0 
				begin
					
					update 
						FEX_TEMP..Transfer_Inventory
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	
					
					
					update 
						FEX_TEMP..Transfer_InventoryEquipment
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	
					
					update 
						FEX_TEMP..Transfer_InventoryService
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	
					
					update 
						FEX_TEMP..Transfer_InventoryTitle
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	


					update 
						FEX_TEMP..Transfer_InsPolicies
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	

					-- inventoryequipment
					insert
						InventoryEquipment
					select
						@To_Inst_ID,
						[New_Inventory_ID], 
						[Equipment_Id], 
						[CustomDesc]
					from
						FEX_TEMP..Transfer_InventoryEquipment (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Inventory_ID = @New_Inventory_ID	
				

					-- inventoryservice
					insert
						InventoryService
					select
						@To_Inst_ID,
						[New_Inventory_ID], 
						[New_ServiceComp_ID], 
						[ServiceDate], 
						[PostDate], 
						[PostedBy], 
						[Estimate], 
						[FinalCost], 
						[Status], 
						[RefNumber], 
						[Payment], 
						[PayRefNumber], 
						[Paid], 
						[PaidDate], 
						[PaidBy], 
						[Description], 
						[ExportGUID], 
						[ClassCode], 
						[DTUser_ID],
						[Deleted],
						[PaymentForm_ID]
					from
						FEX_TEMP..Transfer_InventoryService (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Inventory_ID = @New_Inventory_ID	

					-- inventorytitle
					insert
						InventoryTitle
					select
						@To_Inst_ID,
						[New_Inventory_ID], 
						[TitleStatus_ID], 
						[TitleComments], 
						[New_TitleLocation_ID], 
						[TitleNumber], 
						[COTName], 
						[COTAdd1], 
						[COTAdd2], 
						[COTCity], 
						[COTCounty], 
						[COTState], 
						[COTZip], 
						[COTCountry], 
						[COTPhone1], 
						[COTFax1], 
						[LienAcct_ID], 
						[LienBalance], 
						[LienName], 
						[LienAcctNumber], 
						[LienContact], 
						[LienAdd1], 
						[LienAdd2], 
						[LienCity], 
						[LienCounty], 
						[LienState], 
						[LienZip], 
						[LienCountry], 
						[LienPhone1], 
						[LienFax1], 
						[LastModified], 
						[AddDate], 
						[AltTitleLocation],
						[VendorChannel_ID],
						PrevTitleTransferred,
						 PrevTitleSent,
						 PrevTitleNumber,
						 PrevTitleState,
						 PrevPostalCode,
						 PrevCity,
						 PrevState,
						 PrevCounty,
						 CAST('1900-01-01 00:00:00.000' AS DATETIME)  
					from
						FEX_TEMP..Transfer_InventoryTitle (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Inventory_ID = @New_Inventory_ID	

				end
				
			
				set @SuccessCount = @SuccessCount + 1
				
			END TRY
			BEGIN CATCH
				-- Execute error retrieval routine.
				insert
					FEX_TEMP..transfer_errors
				values
					(
						@Transfer_ID,
						'Transfer_Collateral',
						@Old_Inventory_ID,
						Error_Message()
					)
				set @CollateralTransferStatus = 'E'
			END CATCH


		
		end	-- end test for no more person transfer records
	
	end -- end person loop
	
	update
		FEX_TEMP..Transfer
	set
		Collateral_Status = @CollateralTransferStatus,
		Collateral_TI_Count = @SuccessCount
	where
		Transfer_ID = @Transfer_ID
















