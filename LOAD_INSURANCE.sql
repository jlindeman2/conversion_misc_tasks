USE [FEX_PROCESS];

--RUN THIS SELECT QUERY FIRST, AND COPY THE TOP SESSIONGUID IN THE RESULTS---------------
SELECT *
FROM DTS_MASTER..SecuritySessions
WHERE DTUser_ID = '8728'
ORDER BY LastModified DESC;
-----------------------------------------------------------------------------------------

DECLARE @Inst_ID AS INT;
DECLARE @ImportInsurance AS BIT;
DECLARE @SessionGUID AS VARCHAR(100);

--**Enter Inst_ID to be updated and uncomment
SET @Inst_ID = 106186; --CHANGE TO DEALER'S INST_ID
SET @SessionGUID = '12B590CA-7FE7-454F-9715-F830A2D3BF01'; --CHANGE THIS TO THE SESSIONGUID COPIED FROM ABOVE
SET @ImportInsurance = 1;


IF isnull(@ImportInsurance, 0) = 1
   AND isnull(@Inst_ID, 0) > 0
   AND ISNULL(@SessionGUID, '') <> ''
    BEGIN
        DECLARE @Temp_ID INT;
        DECLARE @bTest BIT;
        DECLARE @Curr_ID INT;
        DECLARE @StockNumber VARCHAR(50);
        DECLARE @PolicyNumber VARCHAR(50);
        DECLARE @ColDeduct MONEY;
        DECLARE @ComDeduct MONEY;
        DECLARE @EffDate SMALLDATETIME;
        DECLARE @ExpDate SMALLDATETIME;
        DECLARE @InsCompanyName VARCHAR(80);
        DECLARE @AgentName VARCHAR(50);
        DECLARE @AgentAdd1 VARCHAR(50);
        DECLARE @AgentCity VARCHAR(35);
        DECLARE @AgentState VARCHAR(2);
        DECLARE @AgentZip VARCHAR(10);
        DECLARE @AgentPhone1 VARCHAR(15);
        DECLARE @AgentFax1 VARCHAR(15);
        DECLARE @Acct_ID INT;
        SET @bTest = 1;
        SET @Curr_ID = 0;
        WHILE @bTest = 1
            BEGIN
                SELECT TOP 1 @Temp_ID = ID,
                             @StockNumber = StockNumber,
                             @PolicyNumber = PolicyNumber,
                             @ColDeduct = ColDeduct,
                             @ComDeduct = ComDeduct,
                             @EffDate = EffDate,
                             @ExpDate = ExpDate,
                             @InsCompanyName = InsCompanyName,
                             @AgentName = AgentName,
                             @AgentAdd1 = AgentAdd1,
                             @AgentCity = AgentCity,
                             @AgentState = AgentState,
                             @AgentZip = AgentZip,
                             @AgentPhone1 = AgentPhone1,
                             @AgentFax1 = AgentFax1
                FROM __Import_Acct_Insurance
                WHERE ID > @Curr_ID
                ORDER BY ID;
                IF isnull(@Temp_ID, 0) = 0
                    BEGIN
                        SET @BTest = 0;
                    END;
                ELSE
                    BEGIN
                        SELECT TOP 1 @acct_id = acct_id
                        FROM 
			
                        --**Enter database
                        FEX_001..vwCustomQuery_v4_Accounts_Master_r1
                        WHERE Col_StockNumber = @StockNumber
                              AND inst_id = @Inst_ID;
                        IF isnull(@AgentName, '') = ''
                            BEGIN
                                SET @AgentName = 'Unknown Agent';
                            END;
                        IF isnull(@InsCompanyName, '') = ''
                            BEGIN
                                SET @InsCompanyName = 'Unknown Company';
                            END;
                        SET @AgentPhone1 = REPLACE(@AgentPhone1, ' ', '');
                        SET @AgentPhone1 = REPLACE(@AgentPhone1, '(', '');
                        SET @AgentPhone1 = REPLACE(@AgentPhone1, ')', '');
                        SET @AgentPhone1 = REPLACE(@AgentPhone1, '-', '');
                        SET @AgentPhone1 = REPLACE(@AgentPhone1, '.', '');
                        SET @AgentFax1 = REPLACE(@AgentFax1, ' ', '');
                        SET @AgentFax1 = REPLACE(@AgentFax1, '(', '');
                        SET @AgentFax1 = REPLACE(@AgentFax1, ')', '');
                        SET @AgentFax1 = REPLACE(@AgentFax1, '-', '');
                        SET @AgentFax1 = REPLACE(@AgentFax1, '.', '');
                        SET @AgentZip = REPLACE(@AgentZip, '-', '');			

		

                        --**Enter database and uncomment exec
                        EXEC FEX_001..pr_Acct_v4_Action_UpdateInsurance_r1
                             @Inst_ID,
                             @Acct_ID,
                             0,
			
                             --**Enter SessionGUID
                             @SessionGUID, --@SessionGUID as varchar(50),
			
                             4,
                             0, --@InsCompany_ID as int = 0
                             @InsCompanyName,
                             '', --@NewInsCompanyPhone1 as varchar(50)='',
                             '', --@NewInsCompanyFax1 as varchar(50)='',
                             0, --@InsAgent_ID as int = 0,
                             @AgentName,
                             @AgentPhone1,
                             @AgentFax1,
                             @PolicyNumber,
                             @EffDate,
                             @ExpDate,
                             @ComDeduct,
                             @ColDeduct;
                    END;
                SET @Curr_ID = @Temp_ID;
                SET @Temp_ID = 0;
                CONTINUE;
            END;
    END;
