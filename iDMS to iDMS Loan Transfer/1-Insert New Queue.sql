--Step 1: Insert a new queue
USE FEX_TEMP;
GO
DECLARE @bInsertTransfer AS BIT;
DECLARE @To_Inst_ID AS INT;
DECLARE @To_Portfolio_Id AS INT;
DECLARE @To_Branch_Id AS INT;
DECLARE @To_Source_Id AS INT;
DECLARE @To_Lender_Id AS INT;
DECLARE @To_SQLCatalog AS VARCHAR(50);
DECLARE @From_Inst_ID AS INT;
DECLARE @From_PortfolioList AS VARCHAR(50);
DECLARE @From_BranchList AS VARCHAR(50);
DECLARE @From_SQLCatalog AS VARCHAR(50);

SET @bInsertTransfer = 1;			--Set to 1 to insert a new queue, 0 to check status of existing queues	

-- Source variables
SET @From_Inst_ID = 104009;				--Source Institution ID
SET @From_PortfolioList = '';		--Source portfolio ID(s)
SET @From_BranchList = '575224';		--Source branch ID(s)
-- Destination variables
		
SET @To_Inst_ID = 106339;			--Destination Institution ID 
SET @To_Portfolio_Id = 0;			--Destination Portfolio ID
SET @To_Branch_Id = 1016070;			--Destination Branch ID
SET @To_Source_Id = 1016070;			--Destination Source ID
SET @To_Lender_Id = 1016070;			--Destination Lender ID




SELECT @To_SQLCatalog = sqlcatalog
FROM dts_master..institutions
WHERE inst_id = @To_Inst_ID;
SELECT @From_SQLCatalog = sqlcatalog
FROM dts_master..institutions
WHERE inst_id = @From_Inst_ID;
IF @bInsertTransfer = 1
    BEGIN
        INSERT INTO Transfer
        VALUES
        ('A',
         'O',
         @From_Inst_iD,
         @From_PortfolioList, -- portfolio list
         @From_BranchList, --branch list
         '',
         @To_Inst_ID, -- to inst_id
         @To_Portfolio_ID, -- to portfolio
         @To_Lender_ID, -- to lender_id
         @To_Source_ID, -- to source_
         @To_Branch_ID, -- to branch id
         '',
         0,
         0, -- channel
         '',
         0,
         0, -- person
         '',
         0,
         0, -- account
         '',
         0,
         0, -- collateral
         '',
         0,
         0, -- inventory
         '',
         0,
         0, -- note
         0,
         0
        );
        SELECT @@Identity AS Transfer_ID,
               @From_SQLCatalog AS From_Catalog,
               @To_SQLCatalog AS To_Catalog;
    END;
ELSE
    BEGIN
        SELECT @From_SQLCatalog AS From_Catalog,
               @To_SQLCatalog AS To_Catalog;
        SELECT *
        FROM FEX_TEMP..Transfer
        ORDER BY Transfer_ID DESC;
    END;
