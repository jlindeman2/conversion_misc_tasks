--Step 2: Reset and Transfer out
USE FEX_TEMP;
GO
DECLARE @Transfer_ID AS INT;
--DECLARE @bReset AS BIT;
SET @Transfer_ID = 140;				    --Set ID from Step one
UPDATE fex_temp..Transfer
  SET
      Status = 'O',
      ChannelStatus = '',
      channel_to_count = 0,
      channel_ti_count = 0,
      AccountStatus = '',
      Account_TI_Count = 0,
      Account_TO_Count = 0,
      PersonStatus = '',
      Person_TI_Count = 0,
      Person_TO_Count = 0,
      Collateral_Status = '',
      collateral_TI_Count = 0,
      collateral_TO_Count = 0,
      inventoryStatus = '',
      inventory_TI_Count = 0,
      inventory_TO_Count = 0,
      noteStatus = '',
      note_TI_Count = 0,
      note_TO_Count = 0
WHERE Transfer_ID = @Transfer_ID;
DELETE FEX_TEMP..Transfer_Accounts
WHERE Transfer_ID = @Transfer_ID;
DELETE FEX_TEMP..Transfer_Channels
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctAmendment
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctBalances
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctBalanceSummary
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctInterestSchedule
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctPaymentHistory
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctPaymentSchedule
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctPaymentSchedules
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_AcctRepos
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_Errors
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_InsPolicies
WHERE Transfer_ID = @Transfer_ID;
DELETE FEX_TEMP..Transfer_Inventory
WHERE Transfer_ID = @Transfer_ID;
DELETE FEX_TEMP..Transfer_InventoryEquipment
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_InventoryService
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_InventoryTitle
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_Note
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_NoteLarge
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_Person
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_PersonAccount
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_PersonAddress
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_PersonEmployment
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_PersonPhone
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_PersonReference
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_TranSplits
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_TransactionAccountSnapshot
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_TransactionSummary
WHERE Transfer_ID = @Transfer_ID;
DELETE fex_temp..Transfer_Transactions
WHERE Transfer_ID = @Transfer_ID;


SELECT *
FROM fex_temp..transfer
ORDER BY transfer_id DESC;