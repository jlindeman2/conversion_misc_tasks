﻿--SET SQL CATALOG (DTS_001,DTS_002,FEX_001, ETC.)
declare @Inst_ID as int
declare @bDestroy as bit
declare @bLoad as BIT

set @Inst_ID = 106207  --CHANGE TO YOUR DEALER'S INST_ID
set @bLoad = 0 --SET TO 1 TO BEGIN DATA PUSH, 0 TO CHECK THE LOG

set @bDestroy = 0 --NEVER SET TO 1 UNLESS SPECIFIED

select sqlcatalog,Name from dts_master..institutions where inst_id = @Inst_ID
select 'Below' AS OpenStatusCount
select count(inst_id) from fex_process..omnique_queue where status = 'O' AND inst_id = @Inst_ID
select 'Below' AS CompletedStatusCount
select count(inst_id) from fex_process..omnique_queue where status = 'C' AND inst_id = @Inst_ID
select 'Below' AS ErrorStatusCount
select count(inst_id) from fex_process..omnique_queue where status = 'E' AND inst_id = @Inst_ID
SELECT 'Below' AS ServiceTicketCount
select count(inst_id) from iserviceticket where inst_id = @Inst_ID
SELECT 'Below' AS VendorRecordMetaDataCount
select count(inst_id) from vendorrecordmetadata where inst_id = @Inst_ID
SELECT 'Below' AS TotalQueueCount
select count(inst_id) from fex_process..omnique_queue where inst_id = @Inst_ID 

--SELECT * FROM FEX_PROCESS..Omnique_Queue WHERE Status = 'C' ORDER BY Queue_ID DESC  
--SELECT * FROM FEX_PROCESS..Omnique_Queue WHERE Record_ID = 239683 AND Inst_ID = @Inst_ID
--SELECT * FROM inventory WHERE StockNumber = 'P13319' AND Inst_ID = @Inst_ID
if isnull(@bDestroy,0) = 1
BEGIN


  delete iserviceticket where inst_id = @Inst_ID
  delete vendorrecordmetadata where inst_id = @Inst_ID
  delete fex_process..omnique_queue where inst_id = @Inst_ID

  select 'Detroyed'
end
else
begin
  select * from iserviceticket where inst_id = @Inst_ID
  select * from vendorrecordmetadata where inst_id = @Inst_ID
  select * from fex_process..omnique_queue where inst_id = @Inst_ID order by StatusDate
  --select * from FEX_PROCESS..Omnique_Queue where Inst_ID = @Inst_ID and Record_ID = 3480429
  --select * from FEX_PROCESS..Omnique_Queue where Inst_ID = @Inst_ID and Record_ID = 5769978
  --select * from fex_001..person where person_id = 5789128
  --select * from fex_001..person where Inst_ID = @Inst_ID and LName = 'VILLEGAS'

  --select * from dts_master..recordtypes



end

--select 'Settings'
--select * from dts_master..vendorurl where vendor_id = 900
--select * from dts_master..vendorinstassignmentsmetadata 
--select * from dts_master..vendorinstassignmentsmetadatalayout 

if isnull(@bLoad,0) = 1
begin

  select 'Loading'
  insert fex_process..Omnique_queue
  select 
    i.inst_id,
    1 as RecordType_ID,
    i.inventory_id,
    1 as Action_ID,
    'O' as Status,
    getdate(),
    getdate(),
    'Pending',
    '' as Lock,
    NULL AS vendorassignedcode
     
  from 
    inventory i
    left join dts_master..invstatuses invs (nolock) on invs.status = i.status
  where
  i.inst_id = @Inst_ID and
  invs.actstatus not in ('D','E')
end