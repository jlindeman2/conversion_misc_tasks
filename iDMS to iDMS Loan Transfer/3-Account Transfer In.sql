

USE fex_001

	DECLARE
	@Transfer_ID INT = 149, --UPDATE THIS TO YOUR TRANSFER ID BEFORE RUNNING
	@Execute bit = 1 -- if this is 0 it will give a count of accounts

declare @To_Inst_ID as int
declare @From_Inst_ID as int
declare @SQLCatalog as varchar(50)


select * from FEX_TEMP..Transfer where Transfer_ID = @Transfer_ID

if ISNULL(@Execute,0) = 1
begin


	Update
		FEX_TEMP..Transfer
	set
		@To_Inst_ID = To_Inst_ID,
		Status = 'P'
	where
		Transfer_ID = @Transfer_ID
		and Status = 'TI'

	if ISNULL(@TO_Inst_ID,0) > 0
	begin
	set nocount on

		-- transfer channels
		print 'Start channel Transfer'
		exec pr_TransferIn_v10_Channels_r1 @Transfer_ID
		print 'End Channel Transfer'


		-- transfer persons
		print 'Start Person Transfer'
		exec pr_TransferIn_v10_Person_r1 @Transfer_ID
		print 'End Person Transfer'



		print 'Start Account Transfer'


declare @To_Portfolio_ID int
declare @To_Branch_ID int
declare @To_Lender_ID int
declare @To_Source_ID int
declare @SuccessCount as int
declare @AccountTransferStatus as varchar(2)
update 
	FEX_TEMP..Transfer
set
	@To_Inst_ID = To_Inst_ID,
	@To_Portfolio_ID = To_Portfolio_ID,
	@To_Branch_ID = To_Branch_ID,
	@To_Lender_ID = To_Lender_ID,
	@To_Source_ID = To_Source_ID,
	@From_Inst_ID = From_Inst_ID,
	@SuccessCount = Account_TI_Count,
	AccountStatus = ''
	
where
	Transfer_ID = @Transfer_ID
	
set @SuccessCount = ISNULL(@SuccessCount,0)

if ISNULL(@TO_Inst_ID,0) > 0 
begin

	set @AccountTransferStatus = ''

	declare @bStop_Account as bit
	declare @bStop_Balance as bit
	declare @ErrMessage as varchar(500)
	
	
	/*
	#############################################################
	account transfer
	#############################################################
	*/
	
	declare @New_Acct_ID as int
	--declare @Branch_ID as int
	--declare @Pool_ID as int
	declare @LoanNumber as varchar(30) 
	declare @SalesTaxRate as decimal(7,7)
	declare @Status as char(1) 
	declare @BKDate as datetime
	declare @ClosedDate as smalldatetime
	declare @RepoDate as smalldatetime
	declare @OutRepoDate as smalldatetime
	declare @InactiveDate as smalldatetime
	declare @NoPayDate as smalldatetime
	declare @CODate as smalldatetime
	declare @TakeNoPay as bit
	declare @ReceiveNotice as bit
	declare @Bankrupt as bit
	declare @LastModified as datetime
	declare @AcctType as char(1) 
	declare @FirstPastDueDate as datetime
	declare @CreationMethod_id as int
	--declare @Lender_ID as int
	declare @TicklerDate as datetime
	declare @SalesTaxCounty as varchar(50) 
	declare @TransactionLock as varchar(50) 
	--declare @Deal_ID as int
	--declare @DealWorksheet_ID as int
	--declare @Source_ID as int
	declare @AlertMessage as varchar(1000) 
	declare @OrigCOBalance as money
	declare @OrigCOSalesTaxBalance as money
	declare @CurCOBalance as money
	declare @CurCOSalesTaxBalance as money
	declare @COBalanceCollected as money
	declare @COBalanceAdjusted as money
	declare @COSalesTaxCollected as money
	declare @COSalesTaxAdjusted as money
	declare @SecondaryStatus as varchar(1) 
	declare @LastNoteDate as datetime
	--declare @LastNote_ID as int
	declare @LastMainDate as datetime
	--declare @LastMain_ID as int
	declare @LastPromiseDate as datetime
	--declare @LastPromise_ID as int
	declare @CurDueDate as datetime
	declare @CurDueAmt as money
	declare @FilteredNumDaysPD as int
	declare @ActNumDaysPD as int
	declare @FilteredNumPaymentsPD as int
	declare @ActNumPaymentsPD as int
	declare @FilteredStanding as varchar(1) 
	declare @ActStanding as varchar(1) 
	declare @FilteredPastDueTier as int
	declare @ActPastDueTier as int
	declare @NextDueAmt as money
	declare @NextDueDate as datetime
	declare @LastTransferDate as datetime
	declare @ReportCredit as bit
	declare @FilteredDueDate as datetime
	declare @FilteredDueAmt as money
	declare @PassTimeCustomerNum as varchar(50) 
	declare @ClosingTransactionType as varchar(2) 
	--declare @LastPolicy_ID as int
	--declare @LastRepo_ID as int
	declare @COBalanceAdjusted_ACV as money
	declare @Metro2Status as varchar(6) 
	
	declare @InsNotRequired as bit
	declare @FollowupDate as datetime
	declare @Alert_UseDefaultStyle bit
	declare @Alert_FontColor as varchar(50)
	declare @Alert_BackgroundColor as varchar(50)
	DECLARE @LastRTCDate AS datetime
	
	declare @Old_Acct_ID as int
	declare @Cur_Acct_ID as int
	DECLARE @FirstDelinquencyDate AS DATETIME
	--declare @LastTransfer_ID as int
	
	
	
	set @bStop_Account = 0	
	set @Cur_Acct_ID = 0
	
		
	while @bStop_Account = 0
	begin
	
		set @ErrMessage = ''
		set @New_Acct_ID = 0
		set @Old_Acct_ID = 0
		
		select 
			top 1
			@Old_Acct_ID = acct.Acct_ID,
			--@Inst_ID = acct.Inst_ID,
			--@Acct_ID = acct.Acct_ID,
			--@Branch_ID = acct.Branch_ID,
			--@Pool_ID = acct.Pool_ID,
			@LoanNumber = acct.LoanNumber,
			@SalesTaxRate = acct.SalesTaxRate,
			@Status = acct.Status,
			@BKDate = acct.BKDate,
			@ClosedDate = acct.ClosedDate,
			@RepoDate = acct.RepoDate,
			@OutRepoDate = acct.OutRepoDate,
			@InactiveDate = acct.InactiveDate,
			@NoPayDate = acct.NoPayDate,
			@CODate = acct.CODate,
			@TakeNoPay = acct.TakeNoPay,
			@ReceiveNotice = acct.ReceiveNotice,
			@Bankrupt = acct.Bankrupt,
			@LastModified = acct.LastModified,
			@AcctType = acct.AcctType,
			@FirstPastDueDate = acct.FirstPastDueDate,
			@CreationMethod_id = acct.CreationMethod_id,
			--@Lender_ID = acct.Lender_ID,
			@TicklerDate = acct.TicklerDate,
			@SalesTaxCounty = acct.SalesTaxCounty,
			@TransactionLock = acct.TransactionLock,
			--@Deal_ID = acct.Deal_ID,
			--@DealWorksheet_ID = acct.DealWorksheet_ID,
			--@Source_ID = acct.Source_ID,
			@AlertMessage = acct.AlertMessage,
			@OrigCOBalance = acct.OrigCOBalance,
			@OrigCOSalesTaxBalance = acct.OrigCOSalesTaxBalance,
			@CurCOBalance = acct.CurCOBalance,
			@CurCOSalesTaxBalance = acct.CurCOSalesTaxBalance,
			@COBalanceCollected = acct.COBalanceCollected,
			@COBalanceAdjusted = acct.COBalanceAdjusted,
			@COSalesTaxCollected = acct.COSalesTaxCollected,
			@COSalesTaxAdjusted = acct.COSalesTaxAdjusted,
			@SecondaryStatus = acct.SecondaryStatus,
			@LastNoteDate = acct.LastNoteDate,
			--@LastNote_ID = acct.LastNote_ID,
			@LastMainDate = acct.LastMainDate,
			--@LastMain_ID = acct.LastMain_ID,
			@LastPromiseDate = acct.LastPromiseDate,
			--@LastPromise_ID = acct.LastPromise_ID,
			@CurDueDate = acct.CurDueDate,
			@CurDueAmt = acct.CurDueAmt,
			@FilteredNumDaysPD = acct.FilteredNumDaysPD,
			@ActNumDaysPD = acct.ActNumDaysPD,
			@FilteredNumPaymentsPD = acct.FilteredNumPaymentsPD,
			@ActNumPaymentsPD = acct.ActNumPaymentsPD,
			@FilteredStanding = acct.FilteredStanding,
			@ActStanding = acct.ActStanding,
			@FilteredPastDueTier = acct.FilteredPastDueTier,
			@ActPastDueTier = acct.ActPastDueTier,
			@NextDueAmt = acct.NextDueAmt,
			@NextDueDate = acct.NextDueDate,
			@LastTransferDate = acct.LastTransferDate,
			@ReportCredit = acct.ReportCredit,
			@FilteredDueDate = acct.FilteredDueDate,
			@FilteredDueAmt = acct.FilteredDueAmt,
			@PassTimeCustomerNum = acct.PassTimeCustomerNum,
			@ClosingTransactionType = acct.ClosingTransactionType,
			--@LastPolicy_ID = acct.LastPolicy_ID,
			--@LastRepo_ID = acct.LastRepo_ID,
			@COBalanceAdjusted_ACV = acct.COBalanceAdjusted_ACV,
			@Metro2Status = acct.Metro2Status,
			@InsNotRequired = isnull(acct.InsNotRequired,0),
			@FollowupDate = acct.FollowupDate,
			@Alert_UseDefaultStyle = isnull(acct.Alert_UseDefaultStyle,1),
			@Alert_FontColor = isnull(acct.Alert_FontColor,''),
			@Alert_BackgroundColor = isnull(acct.Alert_BackgroundColor,''),
			@LastRTCDate = ISNULL(acct.LastRTCDate,''),
			--@LastTransfer_ID = isnull(acct.LastTransfer_id,0)
			@FirstDelinquencyDate = ISNULL(acct.FirstDelinquencyDate,'')
			
		from
			fex_temp..Transfer_Accounts acct (nolock)
		where
			Transfer_ID = @Transfer_ID
			and Inst_ID = @From_Inst_ID
			and isnull(New_Acct_ID,0) = 0
			and Acct_ID > @Cur_Acct_ID
		order by 
			Inst_ID,Acct_ID	
			
		if ISNULL(@Old_Acct_ID,0) = 0
		begin
			set @bStop_Account = 1
		end	
		else
		begin
			set @Cur_Acct_ID = @Old_Acct_ID
			
			BEGIN TRY
			
				insert
					Accounts
				(	
					[Inst_ID], 
					[Branch_ID], 
					[Pool_ID], 
					[LoanNumber], 
					[SalesTaxRate], 
					[Status], 
					[BKDate], 
					[ClosedDate], 
					[RepoDate], 
					[OutRepoDate], 
					[InactiveDate], 
					[NoPayDate], 
					[CODate], 
					[TakeNoPay], 
					[ReceiveNotice], 
					[Bankrupt], 
					[LastModified], 
					[AcctType], 
					[FirstPastDueDate], 
					[CreationMethod_id], 
					[Lender_ID], 
					[TicklerDate], 
					[SalesTaxCounty], 
					[TransactionLock], 
					[Deal_ID], 
					[DealWorksheet_ID], 
					[Source_ID], 
					[AlertMessage], 
					[OrigCOBalance], 
					[OrigCOSalesTaxBalance], 
					[CurCOBalance], 
					[CurCOSalesTaxBalance], 
					[COBalanceCollected], 
					[COBalanceAdjusted], 
					[COSalesTaxCollected], 
					[COSalesTaxAdjusted], 
					[SecondaryStatus], 
					[LastNoteDate], 
					[LastNote_ID], 
					[LastMainDate], 
					[LastMain_ID], 
					[LastPromiseDate], 
					[LastPromise_ID], 
					[CurDueDate], 
					[CurDueAmt], 
					[FilteredNumDaysPD], 
					[ActNumDaysPD], 
					[FilteredNumPaymentsPD], 
					[ActNumPaymentsPD], 
					[FilteredStanding], 
					[ActStanding], 
					[FilteredPastDueTier], 
					[ActPastDueTier], 
					[NextDueAmt], 
					[NextDueDate], 
					[LastTransferDate], 
					[ReportCredit], 
					[FilteredDueDate], 
					[FilteredDueAmt], 
					[PassTimeCustomerNum], 
					[ClosingTransactionType], 
					[LastPolicy_ID], 
					[LastRepo_ID], 
					[COBalanceAdjusted_ACV], 
					[Metro2Status],
					InsNotRequired,
					FollowupDate,
					Alert_UseDefaultStyle,
					Alert_FontColor,
					Alert_BackgroundColor,
					LastTransfer_ID,
					LastRTCDate,
					FirstDelinquencyDate
				)values(
					@To_Inst_ID, 
					@To_Branch_ID, 
					@To_Portfolio_ID, 
					@LoanNumber, 
					@SalesTaxRate, 
					@Status, 
					@BKDate, 
					@ClosedDate, 
					@RepoDate, 
					@OutRepoDate, 
					@InactiveDate, 
					@NoPayDate, 
					@CODate, 
					@TakeNoPay, 
					@ReceiveNotice, 
					@Bankrupt, 
					@LastModified, 
					@AcctType, 
					@FirstPastDueDate, 
					@CreationMethod_id, 
					@To_Branch_ID, --@Lender_ID, 
					@TicklerDate, 
					@SalesTaxCounty, 
					@TransactionLock, 
					0, --@Deal_ID, 
					0, --@DealWorksheet_ID, 
					@To_Source_ID,-- @Source_ID, 
					@AlertMessage, 
					@OrigCOBalance, 
					@OrigCOSalesTaxBalance, 
					@CurCOBalance, 
					@CurCOSalesTaxBalance, 
					@COBalanceCollected, 
					@COBalanceAdjusted, 
					@COSalesTaxCollected, 
					@COSalesTaxAdjusted, 
					@SecondaryStatus, 
					@LastNoteDate, 
					0, --@LastNote_ID, 
					@LastMainDate, 
					0, --@LastMain_ID, 
					@LastPromiseDate, 
					0, --@LastPromise_ID, 
					@CurDueDate, 
					@CurDueAmt, 
					@FilteredNumDaysPD, 
					@ActNumDaysPD, 
					@FilteredNumPaymentsPD, 
					@ActNumPaymentsPD, 
					@FilteredStanding, 
					@ActStanding, 
					@FilteredPastDueTier, 
					@ActPastDueTier, 
					@NextDueAmt, 
					@NextDueDate, 
					@LastTransferDate, 
					@ReportCredit, 
					@FilteredDueDate, 
					@FilteredDueAmt, 
					@PassTimeCustomerNum, 
					@ClosingTransactionType, 
					0, --@LastPolicy_ID, 
					0, --@LastRepo_ID, 
					@COBalanceAdjusted_ACV, 
					@Metro2Status,
					@InsNotRequired,
					@FollowupDate,
					@Alert_UseDefaultStyle,
					@Alert_FontColor,
					@Alert_BackgroundColor,
					0, --@LastTransfer_ID,
					@LastRTCDate,
					@FirstDelinquencyDate
				)
				-- get new idenity
				select @New_Acct_ID = @@IDENTITY
				
		
			
				if ISNULL(@New_Acct_ID,0) > 0 
				begin
					-- update account
					update 
						FEX_TEMP..Transfer_Accounts
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
		
					-- update AcctAmendment
					update 
						FEX_TEMP..Transfer_AcctAmendment
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
						
					-- update AcctAmendment
					update 
						FEX_TEMP..Transfer_AcctBalances
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
						
			
					-- update AcctAmend
					update 
						FEX_TEMP..Transfer_AcctBalanceSummary
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
			
					-- update AcctInterest Schedule
					update 
						FEX_TEMP..Transfer_AcctInterestSchedule
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
						
						
					-- update Acctpaymenthistory
					update 
						FEX_TEMP..Transfer_AcctPaymentHistory
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID						
						
					-- update AcctPaymentSchedule
					update 
						FEX_TEMP..Transfer_AcctPaymentSchedule
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
	
					-- update AcctPaymentSchedules
					update 
						FEX_TEMP..Transfer_AcctPaymentSchedules
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID					

					-- update AcctRepos
					update 
						FEX_TEMP..Transfer_AcctRepos
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
	
	
					-- update Inventory
					update 
						FEX_TEMP..Transfer_Inventory
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
	
					-- update Inventory
					update 
						FEX_TEMP..Transfer_InsPolicies
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
	
					-- update Notes
					update 
						FEX_TEMP..Transfer_Note
					set
						New_Record_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and RecordType_ID = 4
						and Record_ID = @Old_Acct_ID
	
	
					-- update PersonAccount
					update 
						FEX_TEMP..Transfer_PersonAccount
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
	
					-- update TransactionAccountSnapshot
					update 
						FEX_TEMP..Transfer_TransactionAccountSnapshot
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID
	
					-- update Transactions
					update 
						FEX_TEMP..Transfer_Transactions
					set
						New_Acct_ID = @New_Acct_ID,
						New_Pool_ID = @To_Portfolio_ID,
						New_Source_ID = @To_Source_ID,
						New_Branch_ID = @To_Branch_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID	
	
					-- update TransactionSummary
					update 
						FEX_TEMP..Transfer_TransactionSummary
					set
						New_Acct_ID = @New_Acct_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Acct_ID = @Old_Acct_ID	
	

					--#################################################
					-- Separate ID Tables
					--#################################################
				
					-- print 'pr_TransferIn_v10_AcctBalance_r1'
					-- transfer the account balance
					exec pr_TransferIn_v10_AcctBalance_r1	
						@Transfer_ID,
						@From_Inst_ID,
						@To_Inst_ID,
						@New_Acct_ID
						
					-- payments
					-- print 'pr_TransferIn_v10_AcctPayments_r1'
					exec pr_TransferIn_v10_AcctPayments_r1
						@Transfer_ID,
						@From_Inst_ID,
						@To_Inst_ID,
						@New_Acct_ID
					
					-- now do the transactions
					-- print 'pr_TransferIn_v10_AcctPayments_r1'
					exec pr_TransferIn_v10_AcctTransactions_r1
						@Transfer_ID,
						@From_Inst_ID,
						@To_Inst_ID,
						@New_Acct_ID
						
						
					-- bring in collateral
					-- print 'pr_TransferIn_v10_AcctPayments_r1'

	declare @SuccessCount2 int
	declare @CollateralTransferStatus as varchar(2)
	
	set @CollateralTransferStatus = ''
	set @SuccessCount2 = 0
	
	update
		FEX_TEMP..Transfer
	set
		Collateral_Status = '',
		@SuccessCount2 = collateral_TI_count
	where
		Transfer_ID = @Transfer_ID
		
	set @SuccessCount2 = isnull(@SuccessCount2,0)
		
	/*
	#############################################################
	account balance transfer
	#############################################################
	*/
	
		-- accounts
	--print 'transfer In account collateral'
	declare @New_Inventory_ID as int
	declare @Old_Inventory_ID as int
	declare @Cur_Inventory_ID as int
	declare @bStop_Inventory as bit
	declare @ErrMessage2 as varchar(50)
	
	
	declare @New_Sale_ID as int
	declare @New_TISale_ID as int
	declare @New_RepoAcct_ID as int
	declare @New_AcquiredChannel_ID as int
	declare @New_FinanceChannel_ID as int
	declare @New_AcquiredFrom_ID as int
	declare @New_EvaluationGroup_ID as int
	
	
	--declare @Inventory_ID as int
	--declare @Inst_ID as int
	declare @InvType_ID as int
	declare @StockNumber as varchar(50) 
	declare @NewFlag as bit
	declare @Year as smallint
	declare @Make as varchar(30) 
	declare @Model as varchar(30) 
	declare @BodyStyle as varchar(50) 
	declare @ExtColor as varchar(75) 
	declare @ExtTrim as varchar(75) 
	declare @IntColor as varchar(75) 
	declare @VIN as varchar(20) 
	declare @LicPlates as varchar(10) 
	declare @LicPlatesExpire as datetime
	declare @ValNumber as varchar(50) 
	declare @InspectExpire as datetime
	declare @Mileage as int
	declare @MileageStatus_ID as int
	declare @Condition_ID as int
	declare @Transmission as varchar(50) 
	declare @DriveType as varchar(50) 
	declare @Engine as varchar(50) 
	declare @Comments as varchar(2000) 
	declare @AskPrice as money
	declare @AskDown as money
	declare @AskTerm as int
	declare @ImageMain as varchar(50) 
	declare @ImageBack as varchar(50) 
	declare @ImageSide as varchar(50) 
	declare @ImageInterior as varchar(50) 
	declare @Status2 as char(1) 
	--declare @Sale_ID as int
	declare @AcquiredPrice as money
	declare @AcquiredDate as datetime
	declare @AcquiredMethod as char(1) 
	declare @AcquiredMileage as int
	declare @AcquiredMileageStatus_ID as int
	--declare @AcquiredChannel_ID as int
	declare @AcquiredRefNumber as varchar(50) 
	declare @PurchaseStatus as char(1) 
	--declare @TI_Sale_ID as int
	--declare @TI_SaleItem_ID as int
	declare @TI_Payoff as money
	declare @TI_AccountNumber as varchar(25) 
	declare @TI_CustomerName as varchar(50) 
	declare @TI_CompanyName as varchar(50) 
	declare @TI_Add1 as varchar(50) 
	declare @TI_Add2 as varchar(50) 
	declare @TI_City as varchar(30) 
	declare @TI_State as char(2) 
	declare @TI_Zip as varchar(10) 
	declare @TI_Contact as varchar(50) 
	declare @TI_Phone as varchar(15) 
	declare @TI_Fax as varchar(15) 
	declare @TI_Email as varchar(80) 
	--declare @FinanceChannel_ID as int
	declare @FinanceDate as datetime
	declare @FinanceAmount as money
	declare @FinanceCloseDate as datetime
	declare @FinanceCloseRefNumber as varchar(50) 
	declare @Location as varchar(50) 
	declare @TitleLocation as varchar(50) 
	declare @ClearTitle as bit
	declare @DescTitleProblem as varchar(50) 
	declare @DateInvTaxPaid as datetime
	declare @LastModified2 as datetime
	declare @AddDate as datetime
	declare @ASIS as bit
	declare @WholesalePrice as money
	declare @DateToAuction as datetime
	declare @AuctionName as varchar(80) 
	declare @AuctionLane as varchar(50) 
	declare @AuctionNumber as varchar(50) 
	--declare @Lot_ID as int
	declare @InspExpireMth as int
	declare @InspExpireYear as int
	declare @LicPlatesExpireMth as int
	declare @LicPlatesExpireYear as int
	--declare @RepoAcct_ID as int
	--declare @Buyer_ID as int
	declare @PakFee as money
	declare @ListOnWeb as bit
	--declare @Acct_ID as int
	declare @FinanceDueDate as datetime
	declare @InvRecoveryDevice as varchar(100) 
	declare @YearPreviousSale as smallint
	declare @Weight as int
	declare @Notes as varchar(500) 
	declare @WarrantyTerms as varchar(100) 
	declare @InvRecoveryDeviceType_ID as int
	declare @InstalledOptions as varchar(500) 
	declare @FuelType as varchar(50) 
	declare @Stereo as varchar(50) 
	declare @ListOnAutoTrader as bit
	declare @Image1 as varchar(50) 
	declare @Image2 as varchar(50) 
	declare @Image3 as varchar(50) 
	declare @Image4 as varchar(50) 
	declare @Image5 as varchar(50) 
	declare @Image6 as varchar(50) 
	declare @Image7 as varchar(50) 
	declare @Image8 as varchar(50) 
	declare @Image9 as varchar(50) 
	declare @Image10 as varchar(50) 
	declare @Image11 as varchar(50) 
	declare @Image12 as varchar(50) 
	declare @Image13 as varchar(50) 
	declare @Image14 as varchar(50) 
	declare @Image15 as varchar(50) 
	declare @Image16 as varchar(50) 
	declare @Image17 as varchar(50) 
	declare @Image18 as varchar(50) 
	declare @Image19 as varchar(50) 
	declare @Image20 as varchar(50) 
	declare @Image21 as varchar(50) 
	declare @Image22 as varchar(50) 
	declare @Image23 as varchar(50) 
	declare @Image24 as varchar(50) 
	declare @Image25 as varchar(50) 
	declare @LastDateListedWeb as datetime
	declare @LastDateListedAutoTrader as datetime
	declare @PromotionalPrice as money
	declare @PromotionalPayment as money
	declare @PromotionalTerm as int
	declare @SpecialFinancePrice as money
	declare @SpecialFinancePayment as money
	declare @SpecialFinanceTerm as int
	declare @Residual24 as money
	declare @Residual36 as money
	declare @Residual48 as money
	declare @Residual60 as money
	declare @FMV as money
	declare @ACV as money
	declare @CRV as money
	declare @Taxes as money
	declare @InitialChargeOff as money
	declare @OwnerDivision as varchar(50) 
--	declare @AlternateLot_ID as int
	declare @ListOnCarsCom as bit
	declare @LastDateListedCarsCom as datetime
	declare @AskPrice_Low as money
	declare @CarsComImageChanged as bit
	declare @TempDealerTag as varchar(50) 
	declare @TempDealerTagIssueDate as datetime
	declare @TempSalesTag as varchar(50) 
	declare @TempSalesTagIssueDate as datetime
	declare @TempSalesExtTag as varchar(50) 
	declare @TempSalesExtTagIssueDate as datetime
	declare @OnTimeContract_ID as int
	declare @TitleNumber as varchar(50) 
	declare @EmmissionsNumber as varchar(50) 
	declare @InvCreationMethod_ID as int
	declare @SellerNotes as varchar(2000) 
	declare @ExtColorCommon as varchar(20) 
	declare @ImageCount as int
	declare @InternetSpecial as bit
	declare @VROXCertificate as varchar(200) 
	declare @Inspected as bit
	declare @InvRecoverySubDeviceType_ID as int
	declare @CertCode as varchar(50) 
	declare @CertVendor_ID as int
	declare @CertStatus as varchar(2) 
	declare @CertDate as datetime
	declare @DeletedDate as datetime
	declare @AcquiredFromType_ID as int
	--declare @AcquiredFrom_ID as int
	declare @TotalExpenses as money
	declare @EvaluationGroup_ID as int
	declare @EvaluationDate as datetime
	declare @EvaluationOfferPrice as money
	declare @EvaluationBidPrice as money
	declare @EvaluationBidExpiration as datetime
	declare @EvaluationLane as varchar(50) 
	declare @EvaluationNumber as varchar(50) 
	declare @ExtPrimaryCommonColor as varchar(50) 
	declare @ExtSecondaryCommonColor as varchar(50) 
	declare @AutoCheck_Requested as BIT
    DECLARE @InvRecoveryDeviceExpDate AS DATETIME
	DECLARE @LastStatus as CHAR(1)
	DECLARE @SiriusStatus_ID AS INT
	
	
	
	
	set @bStop_Inventory = 0	
	set @Cur_Inventory_ID = 0
	--set @Old_Acct_ID = 0
		
	while @bStop_Inventory = 0
	begin
	
		set @ErrMessage2 = ''
		set @New_Inventory_ID = 0
		set @Old_Inventory_ID = 0
		
		select 
			top 1
				@Old_Inventory_ID = inv.Inventory_ID,
				--@Inventory_ID = Inventory_ID,
				--@Inst_ID = inv.Inst_ID,
				@InvType_ID = inv.InvType_ID,
				@StockNumber = inv.StockNumber,
				@NewFlag = inv.NewFlag,
				@Year = inv.Year,
				@Make = inv.Make,
				@Model = inv.Model,
				@BodyStyle = inv.BodyStyle,
				@ExtColor = inv.ExtColor,
				@ExtTrim = inv.ExtTrim,
				@IntColor = inv.IntColor,
				@VIN = inv.VIN,
				@LicPlates = inv.LicPlates,
				@LicPlatesExpire = inv.LicPlatesExpire,
				@ValNumber = inv.ValNumber,
				@InspectExpire = inv.InspectExpire,
				@Mileage = inv.Mileage,
				@MileageStatus_ID = inv.MileageStatus_ID,
				@Condition_ID = inv.Condition_ID,
				@Transmission = inv.Transmission,
				@DriveType = inv.DriveType,
				@Engine = inv.Engine,
				@Comments = inv.Comments,
				@AskPrice = inv.AskPrice,
				@AskDown = inv.AskDown,
				@AskTerm = inv.AskTerm,
				@ImageMain = inv.ImageMain,
				@ImageBack = inv.ImageBack,
				@ImageSide = inv.ImageSide,
				@ImageInterior = inv.ImageInterior,
				@Status2 = inv.Status,
				@New_Sale_ID = inv.New_Sale_ID,
				@AcquiredPrice = inv.AcquiredPrice,
				@AcquiredDate = inv.AcquiredDate,
				@AcquiredMethod = inv.AcquiredMethod,
				@AcquiredMileage = inv.AcquiredMileage,
				@AcquiredMileageStatus_ID = inv.AcquiredMileageStatus_ID,
				@New_AcquiredChannel_ID = inv.New_AcquiredChannel_ID,
				@AcquiredRefNumber = inv.AcquiredRefNumber,
				@PurchaseStatus = inv.PurchaseStatus,
				@New_TISale_ID = inv.New_TISale_ID,
				--@New_TI_SaleItem_ID = 0, --inv.TI_SaleItem_ID,
				@TI_Payoff = inv.TI_Payoff,
				@TI_AccountNumber = inv.TI_AccountNumber,
				@TI_CustomerName = inv.TI_CustomerName,
				@TI_CompanyName = inv.TI_CompanyName,
				@TI_Add1 = inv.TI_Add1,
				@TI_Add2 = inv.TI_Add2,
				@TI_City = inv.TI_City,
				@TI_State = inv.TI_State,
				@TI_Zip = inv.TI_Zip,
				@TI_Contact = inv.TI_Contact,
				@TI_Phone = inv.TI_Phone,
				@TI_Fax = inv.TI_Fax,
				@TI_Email = inv.TI_Email,
				@New_FinanceChannel_ID = inv.New_FinanceChannel_ID,
				@FinanceDate = inv.FinanceDate,
				@FinanceAmount = inv.FinanceAmount,
				@FinanceCloseDate = inv.FinanceCloseDate,
				@FinanceCloseRefNumber = inv.FinanceCloseRefNumber,
				@Location = inv.Location,
				@TitleLocation = inv.TitleLocation,
				@ClearTitle = inv.ClearTitle,
				@DescTitleProblem = inv.DescTitleProblem,
				@DateInvTaxPaid = inv.DateInvTaxPaid,
				@LastModified2 = inv.LastModified,
				@AddDate = inv.AddDate,
				@ASIS = inv.ASIS,
				@WholesalePrice = inv.WholesalePrice,
				@DateToAuction = inv.DateToAuction,
				@AuctionName = inv.AuctionName,
				@AuctionLane = inv.AuctionLane,
				@AuctionNumber = inv.AuctionNumber,
				--@Lot_ID = inv.Lot_ID,
				@InspExpireMth = inv.InspExpireMth,
				@InspExpireYear = inv.InspExpireYear,
				@LicPlatesExpireMth = inv.LicPlatesExpireMth,
				@LicPlatesExpireYear = inv.LicPlatesExpireYear,
				@New_RepoAcct_ID = inv.New_RepoAcct_ID,
				--@New_Buyer_ID = inv.New_Buyer_ID,
				@PakFee = inv.PakFee,
				@ListOnWeb = inv.ListOnWeb,
				--@Acct_ID = inv.New_Acct_ID,
				@FinanceDueDate = inv.FinanceDueDate,
				@InvRecoveryDevice = inv.InvRecoveryDevice,
				@YearPreviousSale = inv.YearPreviousSale,
				@Weight = inv.Weight,
				@Notes = inv.Notes,
				@WarrantyTerms = inv.WarrantyTerms,
				@InvRecoveryDeviceType_ID = inv.InvRecoveryDeviceType_ID,
				@InstalledOptions = inv.InstalledOptions,
				@FuelType = inv.FuelType,
				@Stereo = inv.Stereo,
				@ListOnAutoTrader = inv.ListOnAutoTrader,
				@Image1 = inv.Image1,
				@Image2 = inv.Image2,
				@Image3 = inv.Image3,
				@Image4 = inv.Image4,
				@Image5 = inv.Image5,
				@Image6 = inv.Image6,
				@Image7 = inv.Image7,
				@Image8 = inv.Image8,
				@Image9 = inv.Image9,
				@Image10 = inv.Image10,
				@Image11 = inv.Image11,
				@Image12 = inv.Image12,
				@Image13 = inv.Image13,
				@Image14 = inv.Image14,
				@Image15 = inv.Image15,
				@Image16 = inv.Image16,
				@Image17 = inv.Image17,
				@Image18 = inv.Image18,
				@Image19 = inv.Image19,
				@Image20 = inv.Image20,
				@Image21 = inv.Image21,
				@Image22 = inv.Image22,
				@Image23 = inv.Image23,
				@Image24 = inv.Image24,
				@Image25 = inv.Image25,
				@LastDateListedWeb = inv.LastDateListedWeb,
				@LastDateListedAutoTrader = inv.LastDateListedAutoTrader,
				@PromotionalPrice = inv.PromotionalPrice,
				@PromotionalPayment = inv.PromotionalPayment,
				@PromotionalTerm = inv.PromotionalTerm,
				@SpecialFinancePrice = inv.SpecialFinancePrice,
				@SpecialFinancePayment = inv.SpecialFinancePayment,
				@SpecialFinanceTerm = inv.SpecialFinanceTerm,
				@Residual24 = inv.Residual24,
				@Residual36 = inv.Residual36,
				@Residual48 = inv.Residual48,
				@Residual60 = inv.Residual60,
				@FMV = inv.FMV,
				@ACV = inv.ACV,
				@CRV = inv.CRV,
				@Taxes = inv.Taxes,
				@InitialChargeOff = inv.InitialChargeOff,
				@OwnerDivision = inv.OwnerDivision,
				--@AlternateLot_ID = 0, --inv.AlternateLot_ID,
				@ListOnCarsCom = inv.ListOnCarsCom,
				@LastDateListedCarsCom = inv.LastDateListedCarsCom,
				@AskPrice_Low = inv.AskPrice_Low,
				@CarsComImageChanged = inv.CarsComImageChanged,
				@TempDealerTag = inv.TempDealerTag,
				@TempDealerTagIssueDate = inv.TempDealerTagIssueDate,
				@TempSalesTag = inv.TempSalesTag,
				@TempSalesTagIssueDate = inv.TempSalesTagIssueDate,
				@TempSalesExtTag = inv.TempSalesExtTag,
				@TempSalesExtTagIssueDate = inv.TempSalesExtTagIssueDate,
				@OnTimeContract_ID = inv.OnTimeContract_ID,
				@TitleNumber = inv.TitleNumber,
				@EmmissionsNumber = inv.EmmissionsNumber,
				@InvCreationMethod_ID = inv.InvCreationMethod_ID,
				@SellerNotes = inv.SellerNotes,
				@ExtColorCommon = inv.ExtColorCommon,
				@ImageCount = inv.ImageCount,
				@InternetSpecial = inv.InternetSpecial,
				@VROXCertificate = inv.VROXCertificate,
				@Inspected = inv.Inspected,
				@InvRecoverySubDeviceType_ID = inv.InvRecoverySubDeviceType_ID,
				@CertCode = inv.CertCode,
				@CertVendor_ID = inv.CertVendor_ID,
				@CertStatus = inv.CertStatus,
				@CertDate = inv.CertDate,
				@DeletedDate = inv.DeletedDate,
				@AcquiredFromType_ID = inv.AcquiredFromType_ID,
				@New_AcquiredFrom_ID = inv.New_AcquiredFrom_ID,
				@TotalExpenses = inv.TotalExpenses,
				@EvaluationGroup_ID = 0, --inv.EvaluationGroup_ID,
				@EvaluationDate = null, --inv.EvaluationDate,
				@EvaluationOfferPrice = 0, --inv.EvaluationOfferPrice,
				@EvaluationBidPrice = 0, --inv.EvaluationBidPrice,
				@EvaluationBidExpiration = null, --inv.EvaluationBidExpiration,
				@EvaluationLane = '', --inv.EvaluationLane,
				@EvaluationNumber = '', --inv.EvaluationNumber,
				@ExtPrimaryCommonColor = inv.ExtPrimaryCommonColor,
				@ExtSecondaryCommonColor = inv.ExtSecondaryCommonColor,
				@AutoCheck_Requested = 0, --inv.AutoCheck_Requested
				@InvRecoveryDeviceExpDate = '', --inv.InvRecoveryDeviceExpDate,
				@LastStatus = '', --inv.LastStatus
				@SiriusStatus_ID = 0
		from
			fex_temp..Transfer_Inventory inv (nolock)
		where
			Transfer_ID = @Transfer_ID
			and Inst_ID = @From_Inst_ID
			and isnull(New_Inventory_ID,0) = 0
			and New_Acct_ID = @New_Acct_ID
			and Inventory_ID > @Cur_Inventory_ID
		order by 
			Inst_ID,Acct_ID,Inventory_ID	
			
		if ISNULL(@Old_Inventory_ID,0) = 0
		begin
			set @bStop_Inventory = 1
		end	
		else
		begin
			set @Cur_Inventory_ID = @Old_Inventory_ID
			
			BEGIN TRY
			
				insert
					inventory
				(	
					[Inst_ID], 
					[InvType_ID], 
					[StockNumber], 
					[NewFlag], 
					[Year], 
					[Make], 
					[Model], 
					[BodyStyle], 
					[ExtColor], 
					[ExtTrim], 
					[IntColor], 
					[VIN], 
					[LicPlates], 
					[LicPlatesExpire], 
					[ValNumber], 
					[InspectExpire], 
					[Mileage], 
					[MileageStatus_ID], 
					[Condition_ID], 
					[Transmission], 
					[DriveType], 
					[Engine], 
					[Comments], 
					[AskPrice], 
					[AskDown], 
					[AskTerm], 
					[ImageMain], 
					[ImageBack], 
					[ImageSide], 
					[ImageInterior], 
					[Status], 
					[Sale_ID], 
					[AcquiredPrice], 
					[AcquiredDate], 
					[AcquiredMethod], 
					[AcquiredMileage], 
					[AcquiredMileageStatus_ID], 
					[AcquiredChannel_ID], 
					[AcquiredRefNumber], 
					[PurchaseStatus], 
					[TI_Sale_ID], 
					[TI_SaleItem_ID], 
					[TI_Payoff], 
					[TI_AccountNumber], 
					[TI_CustomerName], 
					[TI_CompanyName], 
					[TI_Add1], 
					[TI_Add2], 
					[TI_City], 
					[TI_State], 
					[TI_Zip], 
					[TI_Contact], 
					[TI_Phone], 
					[TI_Fax], 
					[TI_Email], 
					[FinanceChannel_ID], 
					[FinanceDate], 
					[FinanceAmount], 
					[FinanceCloseDate], 
					[FinanceCloseRefNumber], 
					[Location], 
					[TitleLocation], 
					[ClearTitle], 
					[DescTitleProblem], 
					[DateInvTaxPaid], 
					[LastModified], 
					[AddDate], 
					[ASIS], 
					[WholesalePrice], 
					[DateToAuction], 
					[AuctionName], 
					[AuctionLane], 
					[AuctionNumber], 
					[Lot_ID], 
					[InspExpireMth], 
					[InspExpireYear], 
					[LicPlatesExpireMth], 
					[LicPlatesExpireYear], 
					[RepoAcct_ID], 
					[Buyer_ID], 
					[PakFee], 
					[ListOnWeb], 
					[Acct_ID], 
					[FinanceDueDate], 
					[InvRecoveryDevice], 
					[YearPreviousSale], 
					[Weight], 
					[Notes], 
					[WarrantyTerms], 
					[InvRecoveryDeviceType_ID], 
					[InstalledOptions], 
					[FuelType], 
					[Stereo], 
					[ListOnAutoTrader], 
					[Image1], 
					[Image2], 
					[Image3], 
					[Image4], 
					[Image5], 
					[Image6], 
					[Image7], 
					[Image8], 
					[Image9], 
					[Image10], 
					[Image11], 
					[Image12], 
					[Image13], 
					[Image14], 
					[Image15], 
					[Image16], 
					[Image17], 
					[Image18], 
					[Image19], 
					[Image20], 
					[Image21], 
					[Image22], 
					[Image23], 
					[Image24], 
					[Image25], 
					[LastDateListedWeb], 
					[LastDateListedAutoTrader], 
					[PromotionalPrice], 
					[PromotionalPayment], 
					[PromotionalTerm], 
					[SpecialFinancePrice], 
					[SpecialFinancePayment], 
					[SpecialFinanceTerm], 
					[Residual24], 
					[Residual36], 
					[Residual48], 
					[Residual60], 
					[FMV], 
					[ACV], 
					[CRV], 
					[Taxes], 
					[InitialChargeOff], 
					[OwnerDivision], 
					[AlternateLot_ID], 
					[ListOnCarsCom], 
					[LastDateListedCarsCom], 
					[AskPrice_Low], 
					[CarsComImageChanged], 
					[TempDealerTag], 
					[TempDealerTagIssueDate], 
					[TempSalesTag], 
					[TempSalesTagIssueDate], 
					[TempSalesExtTag], 
					[TempSalesExtTagIssueDate], 
					[OnTimeContract_ID], 
					[TitleNumber], 
					[EmmissionsNumber], 
					[InvCreationMethod_ID], 
					[SellerNotes], 
					[ExtColorCommon], 
					[ImageCount], 
					[InternetSpecial], 
					[VROXCertificate], 
					[Inspected], 
					[InvRecoverySubDeviceType_ID], 
					[CertCode], 
					[CertVendor_ID], 
					[CertStatus], 
					[CertDate], 
					[DeletedDate], 
					[AcquiredFromType_ID], 
					[AcquiredFrom_ID], 
					[TotalExpenses], 
					[EvaluationGroup_ID], 
					[EvaluationDate], 
					[EvaluationOfferPrice], 
					[EvaluationBidPrice], 
					[EvaluationBidExpiration], 
					[EvaluationLane], 
					[EvaluationNumber], 
					[ExtPrimaryCommonColor], 
					[ExtSecondaryCommonColor], 
					[AutoCheck_Requested],
					[InvRecoveryDeviceExpDate],
					[LastStatus],
					[SiriusStatus_ID]
				)values(
					@To_Inst_ID, 
					@InvType_ID, 
					@StockNumber, 
					@NewFlag, 
					@Year, 
					@Make, 
					@Model, 
					@BodyStyle, 
					@ExtColor, 
					@ExtTrim, 
					@IntColor, 
					@VIN, 
					@LicPlates, 
					@LicPlatesExpire, 
					@ValNumber, 
					@InspectExpire, 
					@Mileage, 
					@MileageStatus_ID, 
					@Condition_ID, 
					@Transmission, 
					@DriveType, 
					@Engine, 
					@Comments, 
					@AskPrice, 
					@AskDown, 
					@AskTerm, 
					@ImageMain, 
					@ImageBack, 
					@ImageSide, 
					@ImageInterior, 
					@Status2, 
					@New_Sale_ID, 
					@AcquiredPrice, 
					@AcquiredDate, 
					@AcquiredMethod, 
					@AcquiredMileage, 
					@AcquiredMileageStatus_ID, 
					@New_AcquiredChannel_ID, 
					@AcquiredRefNumber, 
					@PurchaseStatus, 
					@New_TISale_ID, 
					0, --@TI_SaleItem_ID, 
					@TI_Payoff, 
					@TI_AccountNumber, 
					@TI_CustomerName, 
					@TI_CompanyName, 
					@TI_Add1, 
					@TI_Add2, 
					@TI_City, 
					@TI_State, 
					@TI_Zip, 
					@TI_Contact, 
					@TI_Phone, 
					@TI_Fax, 
					@TI_Email, 
					@New_FinanceChannel_ID, 
					@FinanceDate, 
					@FinanceAmount, 
					@FinanceCloseDate, 
					@FinanceCloseRefNumber, 
					@Location, 
					@TitleLocation, 
					@ClearTitle, 
					@DescTitleProblem, 
					@DateInvTaxPaid, 
					@LastModified2, 
					@AddDate, 
					@ASIS, 
					@WholesalePrice, 
					@DateToAuction, 
					@AuctionName, 
					@AuctionLane, 
					@AuctionNumber, 
					@To_Branch_ID, 
					@InspExpireMth, 
					@InspExpireYear, 
					@LicPlatesExpireMth, 
					@LicPlatesExpireYear, 
					@New_RepoAcct_ID, 
					0, ---@New_Buyer_ID, 
					@PakFee, 
					@ListOnWeb, 
					@New_Acct_ID, 
					@FinanceDueDate, 
					@InvRecoveryDevice, 
					@YearPreviousSale, 
					@Weight, 
					@Notes, 
					@WarrantyTerms, 
					@InvRecoveryDeviceType_ID, 
					@InstalledOptions, 
					@FuelType, 
					@Stereo, 
					@ListOnAutoTrader, 
					@Image1, 
					@Image2, 
					@Image3, 
					@Image4, 
					@Image5, 
					@Image6, 
					@Image7, 
					@Image8, 
					@Image9, 
					@Image10, 
					@Image11, 
					@Image12, 
					@Image13, 
					@Image14, 
					@Image15, 
					@Image16, 
					@Image17, 
					@Image18, 
					@Image19, 
					@Image20, 
					@Image21, 
					@Image22, 
					@Image23, 
					@Image24, 
					@Image25, 
					@LastDateListedWeb, 
					@LastDateListedAutoTrader, 
					@PromotionalPrice, 
					@PromotionalPayment, 
					@PromotionalTerm, 
					@SpecialFinancePrice, 
					@SpecialFinancePayment, 
					@SpecialFinanceTerm, 
					@Residual24, 
					@Residual36, 
					@Residual48, 
					@Residual60, 
					@FMV, 
					@ACV, 
					@CRV, 
					@Taxes, 
					@InitialChargeOff, 
					@OwnerDivision, 
					0, --@AlternateLot_ID, 
					@ListOnCarsCom, 
					@LastDateListedCarsCom, 
					@AskPrice_Low, 
					@CarsComImageChanged, 
					@TempDealerTag, 
					@TempDealerTagIssueDate, 
					@TempSalesTag, 
					@TempSalesTagIssueDate, 
					@TempSalesExtTag, 
					@TempSalesExtTagIssueDate, 
					@OnTimeContract_ID, 
					@TitleNumber, 
					@EmmissionsNumber, 
					@InvCreationMethod_ID, 
					@SellerNotes, 
					@ExtColorCommon, 
					@ImageCount, 
					@InternetSpecial, 
					@VROXCertificate, 
					0, -- cannot be null @Inspected, 
					@InvRecoverySubDeviceType_ID, 
					@CertCode, 
					@CertVendor_ID, 
					@CertStatus, 
					@CertDate, 
					@DeletedDate, 
					@AcquiredFromType_ID, 
					@New_AcquiredFrom_ID, 
					isnull(@TotalExpenses,0), 
					@EvaluationGroup_ID, 
					@EvaluationDate, 
					@EvaluationOfferPrice, 
					@EvaluationBidPrice, 
					@EvaluationBidExpiration, 
					@EvaluationLane, 
					@EvaluationNumber, 
					@ExtPrimaryCommonColor, 
					@ExtSecondaryCommonColor, 
					@AutoCheck_Requested,
					@InvRecoveryDeviceExpDate,
					@LastStatus,
					@SiriusStatus_ID
				)
				-- get new idenity
				select @New_Inventory_ID = @@IDENTITY
				
		
			
				if ISNULL(@New_Inventory_ID,0) > 0 
				begin
					
					update 
						FEX_TEMP..Transfer_Inventory
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	
					
					
					update 
						FEX_TEMP..Transfer_InventoryEquipment
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	
					
					update 
						FEX_TEMP..Transfer_InventoryService
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	
					
					update 
						FEX_TEMP..Transfer_InventoryTitle
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	


					update 
						FEX_TEMP..Transfer_InsPolicies
					set
						New_Inventory_ID = @New_Inventory_ID
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID  = @From_Inst_ID
						and Inventory_ID = @Old_Inventory_ID	

					-- inventoryequipment
					insert
						InventoryEquipment
					select
						@To_Inst_ID,
						[New_Inventory_ID], 
						[Equipment_Id], 
						[CustomDesc]
					from
						FEX_TEMP..Transfer_InventoryEquipment (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Inventory_ID = @New_Inventory_ID	
				

					-- inventoryservice
					insert
						InventoryService
					select
						@To_Inst_ID,
						[New_Inventory_ID], 
						[New_ServiceComp_ID], 
						[ServiceDate], 
						[PostDate], 
						[PostedBy], 
						[Estimate], 
						[FinalCost], 
						[Status], 
						[RefNumber], 
						[Payment], 
						[PayRefNumber], 
						[Paid], 
						[PaidDate], 
						[PaidBy], 
						[Description], 
						[ExportGUID], 
						[ClassCode], 
						[DTUser_ID],
						[Deleted],
						[PaymentForm_ID]
					from
						FEX_TEMP..Transfer_InventoryService (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Inventory_ID = @New_Inventory_ID	

					-- inventorytitle
					insert
						InventoryTitle
					select
						@To_Inst_ID,
						[New_Inventory_ID], 
						[TitleStatus_ID], 
						[TitleComments], 
						[New_TitleLocation_ID], 
						[TitleNumber], 
						[COTName], 
						[COTAdd1], 
						[COTAdd2], 
						[COTCity], 
						[COTCounty], 
						[COTState], 
						[COTZip], 
						[COTCountry], 
						[COTPhone1], 
						[COTFax1], 
						[LienAcct_ID], 
						[LienBalance], 
						[LienName], 
						[LienAcctNumber], 
						[LienContact], 
						[LienAdd1], 
						[LienAdd2], 
						[LienCity], 
						[LienCounty], 
						[LienState], 
						[LienZip], 
						[LienCountry], 
						[LienPhone1], 
						[LienFax1], 
						[LastModified], 
						[AddDate], 
						[AltTitleLocation],
						[VendorChannel_ID],
						[PrevTitleTransferred],
						[PrevTitleSent],
						[PrevTitleNumber],
						[PrevTitleState],
						[PrevPostalCode],
						[PrevCity],
						[PrevState],
						[PrevCounty],
						[PrevTitleReceived]
					from
						FEX_TEMP..Transfer_InventoryTitle (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Inventory_ID = @New_Inventory_ID	

				end
				
			
				set @SuccessCount2 = @SuccessCount2 + 1
				
			END TRY
			BEGIN CATCH
				-- Execute error retrieval routine.
				insert
					FEX_TEMP..transfer_errors
				values
					(
						@Transfer_ID,
						'Transfer_Collateral',
						@Old_Inventory_ID,
						Error_Message()
					)
				set @CollateralTransferStatus = 'E'
			END CATCH


		
		end	-- end test for no more person transfer records
	
	end -- end person loop
	
	update
		FEX_TEMP..Transfer
	set
		Collateral_Status = @CollateralTransferStatus,
		Collateral_TI_Count = @SuccessCount2
	where
		Transfer_ID = @Transfer_ID


					--#################################################
					-- Separate ID Tables
					--#################################################


				
				
				
					--#########################################
					-- create new records
					--#########################################
				
						
					-- now insert AcctRepos
					-- print 'AcctAmendment'
					
					insert
						AcctAmendment
					select
						SnapshotType,
						@To_Inst_ID,
						New_Acct_ID,
						New_Balance_ID, 
						[AmendmentDesc], 
						[AmendmentDate], 
						[LoanRate], 
						[RegZAPR], 
						[CurBalance], 
						[CurInterestBalance], 
						[CurSalesTaxBalance], 
						[CurDownPaymentBalance], 
						[CurNonInterestBalance], 
						[DueInterest], 
						[DueLateFees], 
						[CurMiscFeeBalance], 
						[NextAccrualDate], 
						[SystemDate], 
						[SnapshotDate], 
						[New_AmendmentTran_ID],
						[FinanceCharge]
					from
						FEX_TEMP..Transfer_AcctAmendment (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Acct_ID = @New_Acct_ID
					
		
				
					-- now insert intestet schedule
					-- print 'AcctInterestSchedule'
					insert
						AcctInterestSchedule
					select
						@To_Inst_ID,
						New_Acct_ID,
						New_Balance_ID,
						[Period], 
						[PeriodBegDate], 
						[PeriodEndDate], 
						[DaysInPeriod], 
						[ScheduledPrinBal], 
						[ScheduledBkInt], 
						[RebatePerDiem]
					from
						FEX_TEMP..Transfer_AcctInterestSchedule (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Acct_ID = @New_Acct_ID
		
				
					-- insert AcctPaymentHistory
					-- print 'AcctPaymentHistory'
					insert
						AcctPaymentHistory
					select
						@To_Inst_ID,
						New_Acct_ID,
						[Paid_T0], 
						[Paid_T1], 
						[Paid_T2], 
						[Paid_T3], 
						[Paid_T4], 
						[Paid_T5], 
						[Paid_T6], 
						[Paid_T7], 
						[Paid_T8], 
						[Paid_Total]
					from
						FEX_TEMP..Transfer_AcctPaymentHistory (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Acct_ID = @New_Acct_ID
						
					
					-- now insert paymentschedules schedule
					-- print 'AcctPaymentSchedules'
					insert
						AcctPaymentSchedules
					select
						@To_Inst_ID,
						New_Acct_ID,
						New_Balance_ID,
						[PaymentScheduleNumber], 
						[FirstPaymentDate], 
						[NumberOfPayments], 
						[PaymentFrequency], 
						[DesiredPayment], 
						[NumRegPayments], 
						[RegPaymentAmt], 
						[RegTaxPaymentAmt], 
						[NumFinalPayments], 
						[FinalPaymentAmt], 
						[FinalTaxPaymentAmt], 
						[Status], 
						[RegDiscountPaymentAmt], 
						[FinalDiscountPaymentAmt]
					from
						FEX_TEMP..Transfer_AcctPaymentSchedules (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Acct_ID = @New_Acct_ID
						
				
								
					-- now insert AcctRepos
					-- print 'AcctRepos'
					insert
						AcctRepos
					select
						@To_Inst_ID,
						New_Acct_ID,
						New_RepoAgent_ID, 
						[Note_ID], 
						[PostDate], 
						[PlacedDate], 
						[RepoDate], 
						[CancelDate], 
						[Status], 
						[RepoCharges], 
						[Paid], 
						[PaidRefNumber], 
						[UserCode], 
						[Notes], 
						[OnLot], 
						[OtherLocation], 
						[RepoReason_ID], 
						[FinalStatus], 
						[FinalStatusDate], 
						[DTUser_ID], 
						[RepoMethod_ID],
						RepoEffectDate,
						[VehicleLocation_ID],
						[AcctReposAddressTypes_ID],
            			[Address1],
            			[Address2],
            			[City],
            			[State],
            			[Zip],
						[PersonalProperty],
						[Notified],
						[NotifiedOn],
						[DaysToClaim],
						[ClaimDeadline],
						[Description],
						[Witness1],
						[Witness2],
						[Claimed],
						[ClaimedDate],
						[ClaimedBY]
					from
						FEX_TEMP..Transfer_AcctRepos (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Acct_ID = @New_Acct_ID
					order by
						repo_id
				

						
				
				
						-- now insert InsPolicies
					-- print 'InsPolicies'
					insert
						InsPolicies
					select
						@To_Inst_ID,
						[New_InsCompany_ID], 
						[New_InsAgent_ID], 
						[PolNumber], 
						[ColDeduct], 
						[LiabDeduct], 
						[EffDate], 
						[ExpDate], 
						[Status], 
						[AddDate], 
						[LastModified], 
						[FinalExpDate], 
						[FinalStatusDate], 
						0, --[RenewalPolicy_ID], 
						[UserCode], 
						[New_Inventory_ID], 
						[New_Acct_ID],
						NULL AS CancelDate
					from
						FEX_TEMP..Transfer_InsPolicies (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Acct_ID = @New_Acct_ID
					order by
						Policy_ID
				

				
					-- person account
					-- print 'PersonAccount'
					insert
						PersonAccount
					select
						@To_Inst_ID,
						New_Person_ID, 
						New_Acct_ID, 
						[BorrowerType_ID], 
						[OrderNumber],
						ConsumerInformationIndicator,
						ECOACode,
						BK_Petition_Notification,
						0 AS Bankrupt
					from
						FEX_TEMP..Transfer_PersonAccount (nolock)
					where
						Transfer_ID = @Transfer_ID
						and Inst_ID = @From_Inst_ID
						and New_Acct_ID = @New_Acct_ID
			
			
			
					set @SuccessCount = @SuccessCount + 1
					
					print 'Successful Account: ' + cast(@Old_Acct_ID as varchar(10))
					
				end -- end test for new_acct_id = 0
			END TRY
			BEGIN CATCH
				-- Execute error retrieval routine.
				insert
					FEX_TEMP..transfer_errors
				values
					(
						@Transfer_ID,
						'Transfer_Account',
						@Old_Acct_ID,
						Error_Message()
					)
					set @AccountTransferStatus = 'E'
					print 'Error Account: ' + cast(@Old_Acct_ID as varchar(10))
				
			END CATCH


		
		end	-- end test for no more person transfer records
	
	end -- end person loop
	
	update
		FEX_TEMP..Transfer
	set
		AccountStatus = @AccountTransferStatus,
		Account_TI_Count = @SuccessCount
		WHERE Transfer_ID = @Transfer_ID


	print 'Account Transfer In Count: ' + cast(@SuccessCount as varchar(10))


		print 'End Account Transfer'


		print 'Start Note Transfer'
		exec pr_TransferIn_v10_Note_r1 @Transfer_ID,1
		print 'End Account Transfer'

		print 'Start Account Summary'
		exec pr_TransferIn_v10_AccountSummary_r1 @Transfer_ID
		print 'End Account Summary'


		-- complete the process
		Update
			FEX_TEMP..Transfer
		set
			@To_Inst_ID = To_Inst_ID,
			status = case when PersonStatus = 'E' OR AccountStatus = 'E' or ChannelStatus = 'E' then
				'TI'
			else	
				'C'
			end
			
		where
			Transfer_ID = @Transfer_ID
			and Status = 'P'


		set nocount off
	end			
end -- end test for execute
end